@extends('admin.layouts.app')
@section('content')
<div id="VueJs">
<div class="row wrapper wrapper-content-fs animated fadeInRight">

    <div class="row">
            @if (Session::has('flash_message'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! Session('flash_message') !!}</strong>
            </div>
            @endif
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif  
        <div class="col-lg-6">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title pt-1">
                    <div class="d-flex flex-between ">
                        <h3>
                            New Catagory
                        </h3>
                    </div>
                </div>
                <div class="ibox-content">
                    <input type="hidden" id="category_id">
                    <div class="form-group">
                        <label for="ctg_name">Catagory Title</label>
                        <input class="form-control" type="text" id="ctg_name" oninput="createslug()">
                    </div>


                    <div class="form-group">
                        <label for="slug">Slug</label>
                        <input class="form-control" type="text" id="slug" readonly>
                    </div>

                    <div class="form-group">
                        <label for="meta_title">Meta Title</label>
                        <input class="form-control" type="text" id="meta_title">
                    </div>

                    <div class="form-group">
                        <label for="">Meta Tags</label>
                        <textarea class="form-control resize-none" type="text" id="meta_tags" rows="3"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="">Meta Description</label>
                        <textarea class="form-control resize-none" id="meta_desc" type="text" rows="3"></textarea>
                    </div>


                    <div class="d-flex flex-end">

                        <button class="btn btn-md btn-primary m-0 " v-on:click="addCategory">
                            <i class="fa fa-paper-plane-o"></i> Save
                        </button>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-lg-6">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title pt-1">
                    <div class="d-flex flex-between ">
                        <h3>
                            All Catagories
                        </h3>

                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                
                <div class="ibox-content">
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="10%">#</th>
                                <th width="35%">
                                    <h5>Title</h5>
                                </th>
                                <th width="35%">
                                    <h5>Slug</h5>
                                </th>
                                <th width="20%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(category,index) in categories">
                                <td>@{{index+1}}</td>
                                <td>@{{category.name}}</td>
                                <td>@{{category.slug}}</td>
                                <td>
                                    <div class="d-inline ">
                                        <i class="mr-3 fa fa-trash fs-18 text-danger cursor" v-on:click="deletecategory(category._id)"></i>
                                        <i class="ml-3 fa fa-pencil-square-o fs-18 text-success cursor" v-on:click="setUpdateCategory(category._id)"></i>
                                    </div>
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    <div v-if="loader">
            <div id="loading"></div>
    </div>
</div>
</div>
@endsection

@section('scripts')
<script src="{{asset('/admin/js/lfm.js')}}"></script>
<script>
    $('#lfm').filemanager();
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.min.js" integrity="sha256-chlNFSVx3TdcQ2Xlw7SvnbLAavAQLO0Y/LBiWX04viY=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.js" integrity="sha256-65r7bET363amdMrrizpE4H+N9OWh/aDrLWq2UWoqxlg=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@include('admin.scripts.categoryscript')
@endsection