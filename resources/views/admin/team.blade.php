@extends('admin.layouts.app')
@section('content')
<div class="row wrapper wrapper-content-fs animated fadeInRight">
<div id="VueJs">
    <div class="row">
            @if (Session::has('flash_message'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! Session('flash_message') !!}</strong>
            </div>
            @endif
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif  
        <div class="col-lg-6">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title pt-1">
                    <div class="d-flex flex-between ">
                        <h3>
                            Team Members
                        </h3>
                    </div>
                </div>
                <div class="ibox-content">
                    <form action="/admin/saveTeam" enctype="multipart/form-data" method="post">
                    @csrf
                        <input type="hidden" id="member_id" name="id" value="{{@$member->id}}">
                    <div class="form-group">
                        <label for="author_name">Member Name</label>
                        <input class="form-control" type="text" name="name" id="name" value="{{@$member->name}}">
                    </div>


                    <div class="form-group">
                        <label for="email">Member Email</label>
                        <input class="form-control" type="email" name="email" id="email" value="{{@$member->email}}">
                    </div>

                    <div class="form-group">
                        <label for="email">Designation</label>
                        <input class="form-control" type="text" name="designation" value="{{@$member->designation}}">
                    </div>

                    <div class="form-group">
                        <label for="about_author">About Member</label>
                        <input class="form-control" type="text" name="about" id="about" value="{{@$member->about}}">
                    </div>

                    <div class="input-group">
                        <span class="input-group-btn" v-on:click="showManager">
                          <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                            <i class="fa fa-picture-o"></i> Choose
                          </a>
                        </span>
                        <input id="thumbnail" class="form-control" type="text" name="filepath" value="{{@$member->avatar}}">
                      </div>
                      <img id="holder" style="margin-top:15px;max-height:100px;">


                    <div class="d-flex flex-end">

                        <button class="btn btn-md btn-primary m-0 ">
                            <i class="fa fa-paper-plane-o"></i> Save
                        </button>
                    </div>
                </form>
                </div>

                <hr>
                <hr>
                <div class="ibox-content">
                    <form action="/admin/saveTeamHeading" method="post">
                    @csrf
                        <input type="hidden" name="id" value="{{@$data->id}}">
                    <div class="form-group">
                        <label for="">Main Heading</label>
                                <textarea class="form-control resize-none" type="text" rows="10" cols="50" name="heading">{{@$data->heading}}</textarea>
                    </div>


                    <div class="d-flex flex-end">

                        <button class="btn btn-md btn-primary m-0 ">
                            <i class="fa fa-paper-plane-o"></i> Save
                        </button>
                    </div>
                </form>
                </div>

            </div>
        </div>



        <div class="col-lg-6">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title pt-1">
                    <div class="d-flex flex-between ">
                        <h3>
                            Team Members List
                        </h3>

                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>



                <div class="ibox-content">

                    <div class="d-flex flex-between p-2 border-bottom">
                        <div class="pl-3"> <b>#</b> </div>
                        <div></div>
                        <div> <b>Author Name</b> </div>
                        <div></div>
                        <div></div>
                        <div> <b>Actions</b> </div>
                    </div>
                    @php
                    $counter = 0;
                    @endphp
                  @foreach($team as $member)
                    <div class="d-flex flex-between p-2 border-bottom">
                        <div class="pl-3"> {{$counter+1}} </div>
                        <div></div>
                        <div> {{$member->name}}</div>
                        <div></div>
                        <div></div>
                        <div>
                            <div class="d-flex flex-center ">
                                <a href="/admin/team-member/delete/{{$member->id}}"><i class="mr-3 fa fa-trash fs-18 text-danger cursor" ></i></a>
                                <a href="/admin/team-member/edit/{{$member->id}}"><i class="ml-3 fa fa-pencil-square-o fs-18 text-success cursor"></i></a>
                            </div>
                        </div>
                    </div>
                    @php
                        $counter++;
                    @endphp
                    @endforeach

                   
                </div>
            </div>

            <div class="ibox-content">
                <form action="/admin/saveTeamRightData" method="post">
                @csrf
                    <input type="hidden" name="id" value="{{@$data->id}}">
                <div class="form-group">
                    <div class="form-group">
                        <label for="email">Right Section Heading</label>
                        <input class="form-control" type="text" name="right_section_heading" value="{{@$data->right_section_heading}}">
                    </div>
                    <label for="">Right Section Content</label>
                    <textarea class="form-control resize-none" type="text" rows="10" cols="50" name="right_section_content">{{@$data->right_section}}</textarea>
                </div>


                <div class="d-flex flex-end">

                    <button class="btn btn-md btn-primary m-0 ">
                        <i class="fa fa-paper-plane-o"></i> Save
                    </button>
                </div>
            </form>
            </div>

<hr><hr>
            <div class="ibox-content">
                <form action="/admin/saveTeamLeftData" method="post">
                @csrf
                    <input type="hidden" name="id" value="{{@$data->id}}">
                <div class="form-group">
                    <div class="form-group">
                        <label for="email">Left Section Heading</label>
                        <input class="form-control" type="text" name="left_section_heading" value="{{@$data->left_section_heading}}">
                    </div>
                    <label for="">Left Section Content</label>
                    <textarea class="form-control resize-none" type="text" rows="10" cols="50" name="left_section_content">{{@$data->left_section}}</textarea>
                </div>


                <div class="d-flex flex-end">

                    <button class="btn btn-md btn-primary m-0 ">
                        <i class="fa fa-paper-plane-o"></i> Save
                    </button>
                </div>
            </form>
            </div>

        </div>
    </div>
</div>
</div>
@endsection
@section('scripts')
<script src="{{asset('/admin/js/lfm.js')}}"></script>
<script>
    $('#lfm').filemanager();
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.min.js" integrity="sha256-chlNFSVx3TdcQ2Xlw7SvnbLAavAQLO0Y/LBiWX04viY=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.js" integrity="sha256-65r7bET363amdMrrizpE4H+N9OWh/aDrLWq2UWoqxlg=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@include('admin.scripts.authorscript')
@endsection