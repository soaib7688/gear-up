@extends('admin.layouts.app')
@section('content')
<link href="{{asset('/front/bootstrap-tagsinput-master/src/bootstrap-tagsinput.css')}}" rel="stylesheet" type="text/css">
<script src="{{asset('/front/bootstrap-tagsinput-master/src/bootstrap-tagsinput.js')}}"></script>
<div id="VueJs">
    <div class="row wrapper wrapper-content-fs animated fadeInRight ">

        <div class="ibox float-e-margins ribbon-top">
            <div class="ibox-content mt-1 borderless">

                <div class="d-flex flex-between">
                    <h3>New Post</h3>


                </div>

                <hr class="mt-1">

                <div class="row p-2">

                    <div class="col-lg-9">
                        <input type="hidden" id="post_id">
                        <div class="form-group">
                            <label for="">Title</label>
                            <input class="form-control" type="text" value="{{@$post?$post->name:''}}" id="title"
                                oninput="createslug()" required>
                        </div>

                        <div class="form-group">
                            <label for="">Slug</label>
                            <input class="form-control" type="text" value="{{@$post?$post->slug:''}}" id="slug"
                                required>
                        </div>

                        <div class="form-group">
                            <label for="">Content</label>
                            <textarea id="content" class="form-control my-editor"
                                required>{{@$post?$post->content:''}}</textarea>
                        </div>


                        <div class="form-group">
                            <label for="meta_title">Meta Title</label>
                            <input class="form-control" type="text" value="{{@$post?$post->meta_title:''}}"
                                id="meta_title" name="meta_title" required>
                        </div>

                        <div class="form-group">
                            <label for="">Meta Tags</label>
                            <textarea class="form-control resize-none" type="text" name="meta_tags" id="meta_tags"
                                rows="3" required>{{@$post?$post->meta_tags:''}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="">Meta Description</label>
                            <textarea class="form-control resize-none" id="meta_desc" name="meta_desc" type="text"
                                rows="3" required>{{@$post?$post->meta_description:''}}</textarea>
                        </div>

                    </div>

                    <div class="col-lg-3">

                        <div class="input-group">
                            <span class="input-group-btn" v-on:click="showManager">
                                <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                    <i class="fa fa-picture-o"></i> Choose
                                </a>
                            </span>
                            <input class="form-control" type="text" id="thumbnail"
                                value="{{@$post?$post->cover_img:''}}" name="filepath" required>
                        </div>
                        <img id="holder" style="margin-top:15px;max-height:100px;">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="m-0">Author</h3>
                            </div>
                            <div class="panel-body">
                                <select class="form-control m-b" id="author" required>
                                    <option value="">Select Author</option>
                                    <option v-for="author in authors" :value="author._id"
                                        selected="{{@$post?$post->author->_id:''}}">@{{author.name}}</option>
                                </select>
                            </div>

                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="m-0">Post Catagories</h3>
                            </div>
                            <div class="panel-body" v-for="category in categories">
                                <div class="form-group mb-0 ">
                                    <input type="checkbox" class="checkbox-fs" :id="category._id" :value="category._id"
                                        name="categories[]" :checked="isSelected(category._id)">
                                    <label class="label-fs" :for="category._id">
                                        <h5 class="label-fs--heading">@{{category.name}}</h5>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="d-inline">
                    <button class="btn btn-sm btn-danger mr-2" v-on:click="addpost('draft')"> Draft</button>
                    <button class="btn btn-sm btn-primary" type="submit" value="publish"
                        v-on:click="addpost('published')"> Publish</button>
                </div>
            </div>
        </div>
        <div v-if="loader">
                <div id="loading"></div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script src="{{asset('/admin/js/lfm.js')}}"></script>
<script>
    $('#lfm').filemanager();
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.min.js"
    integrity="sha256-chlNFSVx3TdcQ2Xlw7SvnbLAavAQLO0Y/LBiWX04viY=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.js"
    integrity="sha256-65r7bET363amdMrrizpE4H+N9OWh/aDrLWq2UWoqxlg=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@include('admin.scripts.postscript')
@endsection