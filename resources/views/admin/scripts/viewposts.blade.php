<script>
    let vu = new Vue({
        el: '#VueJs',
        data: {
            posts: [],
            loader:true,
        },
        methods: {
           
            fetchPosts() {
                    let self = this;
                    const auth = {
                        headers: {
                            'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
                        }
                    }
                    var link = 'https://gearupcamera-api.herokuapp.com/v1/blog/post';
                    self.$http.get(link,auth).then(function (response) {
                        self.loader=false;
                        self.posts = response.body;
                        console.log(self.posts);
                        
    
                    }, function (error) {
                        json = JSON.stringify(error.body.message);
                        toastr.error(json);
                    });
    
                },

                deletePost(id){
                    let self = this;
                    const auth = {
                        headers: {
                            'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
                        }
                    }
                    var link = 'https://gearupcamera-api.herokuapp.com/v1/blog/post/status/'+id;
                    let data={
                        "isDelete":true
                        }
                    self.$http.put(link,data,auth).then(function (response) {
                        // 
                        toastr.success('Post deleted Successfully');
                        setTimeout(() => {
                            window.location.reload();
                        }, 1000);
    
                    }, function (error) {
                        json = JSON.stringify(error.body.message);
                        toastr.error(json);
                    });
                }

        },
        
        mounted() {
            this.fetchPosts();
        },
    }); 
</script>
