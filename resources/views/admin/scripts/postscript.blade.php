<script>
        let vu = new Vue({
            el: '#VueJs',
            data: {
                authors: [],
                categories:[],
                cat_ids:[],
                posts:[],
                post:'',
                post_cat_ids:[],
                loader:false,
            },
            methods: {
               
                fetchAuthors() {
                    let self = this;
                    self.loader=true;
                    const auth = {
                        headers: {
                            'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
                        }
                    }
                    var link = 'https://gearupcamera-api.herokuapp.com/v1/blog/author';
                    self.$http.get(link,auth).then(function (response) {
                        self.loader=false;
                        self.authors = response.body;
    
                    }, function (error) {
                        json = JSON.stringify(error.body.message);
                        toastr.error(json);
                    });
    
                },
    
                showManager(){
                    $('#lfm').filemanager();
                },

                fetchCategories() {
                    let self = this;
                    self.loader=true;
                    const auth = {
                        headers: {
                            'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
                        }
                    }
                    var link = 'https://gearupcamera-api.herokuapp.com/v1/blog/category';
                    self.$http.get(link,auth).then(function (response) {
                        self.loader=false;
                        self.categories = response.body;

                        $.each(self.categories, function(key, value) {
                                self.cat_ids.push(value._id);
                            
                        });
    
                    }, function (error) {
                        json = JSON.stringify(error.body.message);
                        toastr.error(json);
                    });
    
                },
                addpost(status){
                    let self=this;
                    
                    var id= $('#post_id').val();
                    if(id){
                        this.updatePost(id,status);
                        return false;
                    }
                    self.loader=true;
                    const auth = {
                        headers: {
                            'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
                        }
                    }
                    let title = $('#title').val();
                    let slug = $('#slug').val();
                    let meta_title = $('#meta_title').val();
                    let meta_desc = $('#meta_desc').val();
                    let meta_tags = $('#meta_tags').val();
                    let author = $('#author').val();
                    let categories = $('input[name="categories[]"]').map(function(){
                        if($(this).prop('checked')){
                            return $(this).val();
                        }
                        }).get();
                    var content = tinymce.get("content").getContent();
                    let cover_img = $('#thumbnail').val();

                    let form = {
                        'name':title,
                        'slug':slug,
                        'meta_title':meta_title,
                        'meta_description':meta_desc,
                        'meta_tags':meta_tags,
                        'content':content,
                        'author_id':author,
                        'category_id':categories,
                        'cover_img':cover_img,
                        'status':status
                    }
                    let link = 'https://gearupcamera-api.herokuapp.com/v1/blog/post';
                    self.$http.post(link,form, auth).then(function (response) {
                        self.loader=false;
                        toastr.success('Post Created Successfully');
                        setTimeout(() => {
                            window.location.reload();
                        }, 1000);
                    }, function (error) {
                        json = JSON.stringify(error.body.message);
                        toastr.error(json);
                    });
                    
                },

                updatePost(id,status){
                    let self=this;
                    self.loader=true;
                    const auth = {
                        headers: {
                            'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
                        }
                    }
                    let title = $('#title').val();
                    let slug = $('#slug').val();
                    let meta_title = $('#meta_title').val();
                    let meta_desc = $('#meta_desc').val();
                    let meta_tags = $('#meta_tags').val();
                    let author = $('#author').val();
                    let categories = $('input[name="categories[]"]').map(function(){
                        if($(this).prop('checked')){
                            return $(this).val();
                        }
                        }).get();
                    var content = tinymce.get("content").getContent();
                    let cover_img = $('#thumbnail').val();

                    let form = {
                        'name':title,
                        'slug':slug,
                        'meta_title':meta_title,
                        'meta_description':meta_desc,
                        'meta_tags':meta_tags,
                        'content':content,
                        'author_id':author,
                        'category_id':categories,
                        'cover_img':cover_img,
                        'status':status
                    }
                    let link = 'https://gearupcamera-api.herokuapp.com/v1/blog/post/'+id;
                    self.$http.put(link,form, auth).then(function (response) {
                        self.loader=false;
                        toastr.success('Post Updated Successfully');
                        setTimeout(() => {
                            window.location.href='/admin/view-posts';
                        }, 1000);
                    }, function (error) {
                        json = JSON.stringify(error.body.message);
                        toastr.error(json);
                    });
                    
                },

                fetSinglePost(id){
                    let self = this;
                    const auth = {
                        headers: {
                            'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
                        }
                    }
                    var link = 'https://gearupcamera-api.herokuapp.com/v1/blog/post/'+id;
                    self.$http.get(link,auth).then(function (response) {
                        self.post = response.body;
                        
                        $.each(self.post.category, function(key, value) {
                                self.post_cat_ids.push(value._id);
                            
                        });
                        
                        
                    }, function (error) {
                        json = JSON.stringify(error.body.message);
                        toastr.error(json);
                    });
                },

                isSelected(id){
                    if(this.post_cat_ids.includes(id)){
                        return true;
                    }
                    return false;
                }

            },
            
    
    
    
            mounted() {
                this.fetchAuthors();
                this.fetchCategories();
                let url = window.location.href;
                let parts = url.split("/");
                if(parts[4]=='post'){
                    if(parts[5]=='edit'){
                        let id = parts[6].slice(1);
                        $('#post_id').val(id);
                        this.fetSinglePost(id);
                    }
                }
            },
        }); 
    </script>


<script>
        var editor_config = {
          path_absolute : "/",
          selector: "textarea.my-editor",
          plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
          ],
          toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
          relative_urls: false,
          file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
      
            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
              cmsURL = cmsURL + "&type=Images";
            } else {
              cmsURL = cmsURL + "&type=Files";
            }
      
            tinyMCE.activeEditor.windowManager.open({
              file : cmsURL,
              title : 'Filemanager',
              width : x * 0.8,
              height : y * 0.8,
              resizable : "yes",
              close_previous : "no"
            });
          }
        };
      
        tinymce.init(editor_config);
      </script>

<script>

    function createslug(){
        let str = $('#title').val();
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
        var to   = "aaaaaeeeeeiiiiooooouuuunc------";
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        $('#slug').val(str);

    }
</script>