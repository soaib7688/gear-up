<script>
    let vu = new Vue({
        el: '#VueJs',
        data: {
            authors: [],
            loader:false,
        },
        methods: {
            createAuthor() {
                let self = this;
                const auth = {
                    headers: {
                        'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
                    }
                }
                var id= $('#author_id').val();
                if(id){
                    this.updateAuthor(id);
                    return false;
                }
                self.loader=true;
                var name = $('#name').val();
                var email = $('#email').val();
                var about = $('#about').val();
                var author_img = $('#thumbnail').val();
                var link = 'https://staging-gearupcamera-api.herokuapp.com/v1/blog/author';
                // let form = new FormData();
                // form.append('name', name);
                // form.append('email', email);
                // form.append('about', about);
                // form.append('author_img', author_img);
                let form = {
                    'name':name,
                    'email':email,
                    'about':about,
                    'profile_img':author_img
                }
                self.$http.post(link, form, auth).then(function (response) {
                    self.loader=false;
                    toastr.success('Author Created Successfully');
                    setTimeout(() => {
                        window.location.reload();
                    }, 1000);
                }, function (error) {
                    json = JSON.stringify(error.body.message);
                    toastr.error(json);
                });
            },
            fetchAuthors() {
                let self = this;
                self.loader=true;
                const auth = {
                    headers: {
                        'content-type':'application/json',
                        'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
                    }
                }
                var link = 'https://gearupcamera-api.herokuapp.com/v1/blog/author';
                self.$http.get(link,auth).then(function (response) {
                    self.loader=false;
                    self.authors = response.body;

                }, function (error) {
                    json = JSON.stringify(error.body.message);
                    toastr.error(json);
                });

            },

            showManager(){
                $('#lfm').filemanager();
            },

            deleteAuthor(id){
                let self = this;
                self.loader = true;
                const auth = {
                    headers: {
                        'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
                    }
                }
                var link = 'https://gearupcamera-api.herokuapp.com/v1/blog/author/status/'+id;
                let data={
                    "isDelete":true
                }
                self.$http.put(link,data,auth).then(function (response) {
                    self.loader = false;
                    toastr.success('Author Deleted Successfully');
                    setTimeout(() => {
                        window.location.reload();
                    }, 1000);

                }, function (error) {
                    json = JSON.stringify(error.body.message);
                    toastr.error(json);
                });
            },

            setUpdateAuthor(id){
                
                let data = this.authors;
                let currentAuthor = data.filter(function(item) {
                    return item._id==id;
                });
                let author = currentAuthor[0];
                console.log(author);
                
                $('#author_id').val(author._id);
                $('#name').val(author.name);
                $('#email').val(author.email);
                $('#about').val(author.about);
                
            },

            updateAuthor(id){
                let self = this;
                self.loader = true;
                const auth = {
                    headers: {
                        'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
                    }
                }
                var id= $('#author_id').val();
                
                var name = $('#name').val();
                var email = $('#email').val();
                var about = $('#about').val();
                var author_img = $('#thumbnail').val();
                var link = 'https://gearupcamera-api.herokuapp.com/v1/blog/author/'+id;
                let form = new FormData();
                form.append('name', name);
                form.append('email', email);
                form.append('about', about);
                form.append('author_img', author_img);
                self.$http.put(link, form, auth).then(function (response) {
                    self.loader = false;
                    toastr.success('Author Updated Successfully');
                    setTimeout(() => {
                        window.location.reload();
                    }, 1000);
                }, function (error) {
                    json = JSON.stringify(error.body.message);
                    toastr.error(json);
                });
            }
        },



        mounted() {
            this.fetchAuthors();
        },
    }); 
</script>