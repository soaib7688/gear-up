<script>
        let vu = new Vue({
            el: '#VueJs',
            data: {
                categories: [],
                loader:true,
            },
            methods: {
                addCategory() {
                    let self = this;
                    const auth = {
                        headers: {
                            'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA',
                            'Content-Type' : 'application/json'
                        }
                    }
                    var id= $('#category_id').val();
                    if(id){
                        this.updateCategory(id);
                        return false;
                    }
                    var title = $('#ctg_name').val();
                    var slug = $('#slug').val();
                    var meta_title = $('#meta_title').val();
                    var meta_tags = $('#meta_tags').val();
                    var meta_desc = $('#meta_desc').val();
                    var link = 'https://gearupcamera-api.herokuapp.com/v1/blog/category';
                    let form ={
                        "name":title,
                        "slug":slug,
                        "meta_title":meta_title,
                        "meta_description":meta_desc,
                        "meta_tags":meta_tags
                    }
                    self.$http.post(link,form, auth).then(function (response) {
                        toastr.success('Category Created Successfully');
                        setTimeout(() => {
                            window.location.reload();
                        }, 1000);
                    }, function (error) {
                        json = JSON.stringify(error.body.message);
                        toastr.error(json);
                    });
                },
                fetchCategories() {
                    let self = this;
                    const auth = {
                        headers: {
                            'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
                        }
                    }
                    var link = 'https://gearupcamera-api.herokuapp.com/v1/blog/category';
                    self.$http.get(link,auth).then(function (response) {
                        self.loader = false;
                        self.categories = response.body;
    
                    }, function (error) {
                        json = JSON.stringify(error.body.message);
                        toastr.error(json);
                    });
    
                },
    
                deletecategory(id){
                    let self = this;
                    const auth = {
                        headers: {
                            'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
                        }
                    }
                    var link = 'https://gearupcamera-api.herokuapp.com/v1/blog/category/status/'+id;
                    let data={
                        "isDelete":true
                    }
                    self.$http.put(link,data,auth).then(function (response) {
                        toastr.success('Category Deleted Successfully');
                        setTimeout(() => {
                            window.location.reload();
                        }, 1000);
    
                    }, function (error) {
                        json = JSON.stringify(error.body.message);
                        toastr.error(json);
                    });
                },
    
                setUpdateCategory(id){
                    let data = this.categories;
                    let currentCategory = data.filter(function(item) {
                        return item._id==id;
                    });
                    let category = currentCategory[0];
                    
                    $('#category_id').val(category._id);
                    $('#ctg_name').val(category.name);
                    $('#slug').val(category.slug);
                    $('#meta_title').val(category.meta_title);
                    $('#meta_desc').val(category.meta_description);
                    $('#meta_tags').val(category.meta_tags);
                    
                },
    
                updateCategory(id){
                    let self = this;
                    const auth = {
                        headers: {
                            'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
                        }
                    }
                    var id= $('#category_id').val();  
                    var title = $('#ctg_name').val();
                    var slug = $('#slug').val();
                    var meta_title = $('#meta_title').val();
                    var meta_tags = $('#meta_tags').val();
                    var meta_desc = $('#meta_desc').val();
                    var link = 'https://gearupcamera-api.herokuapp.com/v1/blog/category/'+id;
                    let form ={
                        "name":title,
                        "slug":slug,
                        "meta_title":meta_title,
                        "meta_description":meta_desc,
                        "meta_tags":meta_tags
                    }
                    self.$http.put(link, form, auth).then(function (response) {
                        toastr.success('Category Updated Successfully');
                        setTimeout(() => {
                            window.location.reload();
                        }, 1000);
                    }, function (error) {
                        json = JSON.stringify(error.body.message);
                        toastr.error(json);
                    });
                }
            },
    
    
    
            mounted() {
                this.fetchCategories();
            },
        }); 
    </script>

    <script>

        function createslug(){
            let str = $('#ctg_name').val();
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();

            // remove accents, swap ñ for n, etc
            var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
            var to   = "aaaaaeeeeeiiiiooooouuuunc------";
            for (var i=0, l=from.length ; i<l ; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }

            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes

            $('#slug').val(str);

        }
    </script>