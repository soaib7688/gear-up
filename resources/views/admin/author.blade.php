@extends('admin.layouts.app')
@section('content')
<div class="row wrapper wrapper-content-fs animated fadeInRight">
<div id="VueJs">
    <div class="row">

        <div class="col-lg-6">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title pt-1">
                    <div class="d-flex flex-between ">
                        <h3>
                            Authors
                        </h3>
                    </div>
                </div>
                <div class="ibox-content">
                    <form action="" enctype="multipart/form-data" v-on:submit.prevent="createAuthor">
                        <input type="hidden" id="author_id">
                    <div class="form-group">
                        <label for="author_name">Author Name</label>
                        <input class="form-control" type="text" name="name" id="name">
                    </div>


                    <div class="form-group">
                        <label for="email">Author Email</label>
                        <input class="form-control" type="email" name="email" id="email">
                    </div>

                    {{-- <div class="form-group">
                        <label for="email">Author Facebook Page</label>
                        <input class="form-control" type="email" id="email">
                    </div>

                    <div class="form-group">
                        <label for="email">Author Twitter Page</label>
                        <input class="form-control" type="email" id="email">
                    </div>

                    <div class="form-group">
                        <label for="email">Author Linkdin Page</label>
                        <input class="form-control" type="email" id="email">
                    </div>

                    <div class="form-group">
                        <label for="email">Author Google Profile</label>
                        <input class="form-control" type="email" id="email">
                    </div> --}}

                    <div class="form-group">
                        <label for="about_author">About Author</label>
                        <input class="form-control" type="text" name="about" id="about">
                    </div>


                    {{-- <div class="form-group">
                        <label for="author_upload">Author Image</label>
                        <input class="form-control d-none" type="file" name="profile_img" id="author_upload">
                    </div> --}}

                    <div class="input-group">
                        <span class="input-group-btn" v-on:click="showManager">
                          <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                            <i class="fa fa-picture-o"></i> Choose
                          </a>
                        </span>
                        <input id="thumbnail" class="form-control" type="text" name="filepath">
                      </div>
                      <img id="holder" style="margin-top:15px;max-height:100px;">


                    <div class="d-flex flex-end">

                        <button class="btn btn-md btn-primary m-0 ">
                            <i class="fa fa-paper-plane-o"></i> Save
                        </button>
                    </div>
                </form>
                </div>

            </div>
        </div>

        <div class="col-lg-6">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title pt-1">
                    <div class="d-flex flex-between ">
                        <h3>
                            Author List
                        </h3>

                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>



                <div class="ibox-content">

                    <div class="d-flex flex-between p-2 border-bottom">
                        <div class="pl-3"> <b>#</b> </div>
                        <div></div>
                        <div> <b>Author Name</b> </div>
                        <div></div>
                        <div></div>
                        <div> <b>Actions</b> </div>
                    </div>

                  
                    <div class="d-flex flex-between p-2 border-bottom" v-for="(author,index) in authors">
                        <div class="pl-3"> @{{index+1}} </div>
                        <div></div>
                        <div> @{{author.name}}</div>
                        <div></div>
                        <div></div>
                        <div>
                            <div class="d-flex flex-center ">
                                <i class="mr-3 fa fa-trash fs-18 text-danger cursor" v-on:click="deleteAuthor(author._id)"></i>
                                <i class="ml-3 fa fa-pencil-square-o fs-18 text-success cursor"  v-on:click="setUpdateAuthor(author._id)"></i>
                            </div>
                        </div>
                    </div>
                    

                    <!-- <table class="table">
                                <thead>
                                <tr>
                                    <th width="10%">#</th>
                                    <th class="d-flex flex-center"><h5>Author Name</h5></th>
                                    <th class="d-flex flex-center" >Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td class="d-flex flex-center">Farrukh Murtaza</td>
                                    <td>
                                        <div class="d-flex flex-center ">
                                            <i class="mr-3 fa fa-trash fs-18 text-danger cursor"></i>
                                            <i class="ml-3 fa fa-pencil-square-o fs-18 text-success cursor"></i> 
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Jacob</td>
                                    <td>
                                        <div class="d-flex flex-center">
                                            <i class="mr-3 fa fa-trash fs-18 text-danger cursor"></i>
                                            <i class="ml-3 fa fa-pencil-square-o fs-18 text-success cursor"></i> 
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Larry</td>
                                    <td>
                                        <div class="d-flex flex-center">
                                            <i class="mr-3 fa fa-trash fs-18 text-danger cursor"></i>
                                            <i class="ml-3 fa fa-pencil-square-o fs-18 text-success cursor"></i> 
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                        </table>
                     -->
                </div>
            </div>
        </div>
        <div v-if="loader">
                <div id="loading"></div>
        </div>
    </div>
</div>
</div>
@endsection
@section('scripts')
<script src="{{asset('/admin/js/lfm.js')}}"></script>
<script>
    $('#lfm').filemanager();
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.min.js" integrity="sha256-chlNFSVx3TdcQ2Xlw7SvnbLAavAQLO0Y/LBiWX04viY=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.js" integrity="sha256-65r7bET363amdMrrizpE4H+N9OWh/aDrLWq2UWoqxlg=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@include('admin.scripts.authorscript')
@endsection