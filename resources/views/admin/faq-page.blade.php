@extends('admin.layouts.app')
@section('content')
<div class="animated fadeInRight">
    <div class="row">
            @if (Session::has('flash_message'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! Session('flash_message') !!}</strong>
            </div>
            @endif
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif  
        <div class="col-lg-6">
            <div class="ibox-title">
                <h3>Frequently Asked Questions</h3>                
            </div>

            <div class="ibox-content">
                <div class="row">
                    <form action="{{url('/admin/savefaqs')}}" method="POST">
                        @csrf
                    <input type="hidden" name="id" value="{{@$faq?$faq->id:''}}">
                    <div class="form-group">
                            <label for="">Choose Main Catagory</label>
                            <select name="category" id="" class="form-control" onchange="getSubcats(this.value)">
                                <option value="">Select</option>
                                @foreach ($parent as $item)
                                @php
                                    if (isset($faq)) {
                                        $selected = $faq->cat?$faq->cat:'';
                                    }else {
                                        $selected = "";
                                    }

                                @endphp
                                    <option value="{{$item->id}}" {{$item->title==$selected?'selected':''}}>{{$item->title}}</option>
                                @endforeach
                            </select>
    
                    </div>

                    <div class="form-group" >
                            <label>Select Sub Category</label>
                            <select name="sub-category" id="subcat" class="form-control">
                                @if(!isset($faq))
                                <option value="">Select</option>
                                @endif
                                @php
                                    if (isset($faq)) {
                                        $maincat = DB::table('faqs_categories')->select('*')->where('title',$faq->cat)->first();
                                        $subcats = DB::table('faqs_categories')->select('*')->where('parent_id',$maincat->id)->get();
                                        $selected = $faq->subcat?$faq->subcat:'';
                                    }else {
                                        $selected = "";
                                    }
                                @endphp
                                @if(isset($faq))
                                    @foreach ($subcats as $subcat)
                                        <option value="{{$subcat->id}}" {{$subcat->title==$selected?'selected':''}}>{{$subcat->title}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="text-danger"></div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Question</label>
                            <input type="text" name="question" value="{{@$faq?$faq->question:''}}" class="form-control">
                            <div class="text-danger"></div>
                        </div>

                        <div class="form-group">
                            <label>Answer</label>
                            <textarea class="form-control my-editor" name="answer" id="my-editor" cols="30" rows="10">{!!@$faq?$faq->answer:''!!}</textarea>
                            <div class="text-danger"> </div>
                        </div>

                        <div class="form-group text-right">
                            <input type="submit" class="btn btn-primary">
                        </div>


                    </div>
                </form>
                </div>
            </div>
        </div>

        <div class="col-lg-6">

            <div class="ibox-title">
                <h3 style="display:inline-block;">All Faqs</h3>
            
                {{-- <span class="ibox-tools">
                    <a href="faqs-list.php" class="btn btn-primary">View All Faqs</a>
                </span> --}}
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-10 col-sm-12 col-lg-offset-1">
                        <form action="{{url('/admin/saveOrder')}}" method="POST">
                            @csrf
                        <div class="form-group">

                            <h3>Please Select Order</h3>

                            <ol class="todo-list " id="sortable">
                                @foreach ($data as $item)
                                    <li class="" style="">
                                        <input type="hidden" name="order[]"  class="form-control" value="{{$item->id}}">
                                        <p>Category- {{$item->cat}}<i class="fa fa-angle-right mx-3"></i>{{$item->subcat}}</p>
                                        {{$item->question}}
                                        <a href="/admin/faq/edit/{{$item->id}}"><i class="fa fa-edit"></i></a>
                                        <a href="/admin/faq/delete/{{$item->id}}"><i class="fa fa-trash"></i></a>
                                    </li>
                                @endforeach

                            </ol>
                        </div>


                        <div class="form-group text-right">
                            <input type="submit" class="btn btn-primary">
                        </div>

                    </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $( function() {
        $( "#sortable" ).sortable();
        $( "#sortable" ).disableSelection();
        } );
    </script>

    <script>
        function getSubcats(id){

			$.ajax({
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type : "get",
            global : "false",
            datatype : "html",
            url:"/getSubCats?id="+id,
            // data:id,
            }).done(function(d){
				
                $('#subcat').html(d);
			});
        }
    </script>

        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        var editor_config = {
          path_absolute : "/",
          selector: "textarea.my-editor",
          plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
          ],
          toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
          relative_urls: false,
          file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
      
            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
              cmsURL = cmsURL + "&type=Images";
            } else {
              cmsURL = cmsURL + "&type=Files";
            }
      
            tinyMCE.activeEditor.windowManager.open({
              file : cmsURL,
              title : 'Filemanager',
              width : x * 0.8,
              height : y * 0.8,
              resizable : "yes",
              close_previous : "no"
            });
          }
        };
      
        tinymce.init(editor_config);
    </script>
@endsection