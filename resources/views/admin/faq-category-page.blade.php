@extends('admin.layouts.app')
@section('content')

<div class="row wrapper wrapper-content-fs animated fadeInRight">

    <div class="row">
            @if (Session::has('flash_message'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! Session('flash_message') !!}</strong>
            </div>
            @endif
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif  
        <div class="col-lg-6">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title pt-1">
                    <div class="d-flex flex-between ">
                        <h3>
                            New Catagory
                        </h3>
                    </div>
                </div>
                <form action="{{url('/admin/faq/categories')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{@$faq_category->id}}">
                <div class="ibox-content">
                    <div class="form-group">
                        <label for="">Choose Main Catagory</label>
                        <select name="parent" id="" class="form-control">
                            <option value="">Select</option>
                            @php
                                if(isset($faq_category)){
                                    $selected = $faq_category->parent_id;
                                }else{
                                    $selected="";
                                }
                            @endphp
                            @foreach ($parent as $item)
                                <option value="{{$item->id}}" {{$selected==$item->id?'selected':''}}>{{$item->title}}</option>
                            @endforeach
                        </select>

                    </div>
                    <div class="form-group">
                        <label for="ctg_name">Catagory Title</label>
                        <input class="form-control" type="text" name="title" id="ctg_name" oninput="createslug()" value="{{@$faq_category->title}}">
                    </div>

                    <div class="d-flex flex-end">

                        <input type="submit" class="btn btn-primary" value="Save">
                    </div>
                </div>
                </form>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title pt-1">
                    <div class="d-flex flex-between ">
                        <h3>
                            All Catagories
                        </h3>

                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>

                <div class="ibox-content">
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="10%">#</th>
                                <th width="35%">
                                    <h5>Title</h5>
                                </th>
                                <th width="20%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($categories as $key =>$value)
                            <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$value->title}}</td>
                                    <td>
                                        <div class="d-inline ">
                                            <a href="{{url('/admin/faqCategory/delete/'.$value->id)}}"><i class="mr-3 fa fa-trash fs-18 text-danger cursor" ></i></a>
                                            <a href="{{url('/admin/faqCategory/edit/'.$value->id)}}"><i class="ml-3 fa fa-pencil-square-o fs-18 text-success cursor" ></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            
                            
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection