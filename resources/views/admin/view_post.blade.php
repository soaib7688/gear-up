@extends('admin.layouts.app')
@section('content')

<div id="VueJs">
<div class="row wrapper wrapper-content-fs animated fadeInRight">

    <div class="ibox float-e-margins">
        <div class="ibox-title pt-0 pb-0">

            <div class="d-flex flex-between">
                <h3 class="m-0 mt-3">All Posts</h3>

                {{-- <a href="{{url('/admin/add-post')}}" class="btn btn-primary mt-1"> <span class="fa fa-plus"></span> New</a> --}}
            </div>
            
        </div>
        <div class="ibox-content mt-1 borderless">
                    
            <div class="panel blank-panel" >

                <div class="panel-heading">

                    <div class="panel-options">

                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1"
                                    aria-expanded="true">All</a></li>
                            {{-- <li class=""><a data-toggle="tab" href="#tab-2"
                                    aria-expanded="false">Publish</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-3" aria-expanded="false">Draft</a>
                            </li>
                            <li class=""><a data-toggle="tab" href="#tab-4" aria-expanded="false">Trash</a>
                            </li> --}}
                        </ul>
                    </div>
                </div>
                
                <div class="panel-body">

                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th width="40%">Title</th>
                                        
                                        <th width="10%" class="text-center" >Author</th>
                                        <th width="10%" class="text-center" >Status</th>
                                        {{-- <th width="10%">Tags</th> --}}
                                        <th width="10%" class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="post in posts">                                                    
                                        <td>@{{post.name}}</td>
                                        <td class="text-center">@{{post.author.name}}</td>
                                        <td class="text-center">@{{post.status}}</td>
                                        {{-- <td>@mdo</td> --}}
                                        <td class="text-center">
                                            <div class="d-inline ">
                                                    <a v-on:click="deletePost(post._id)">
                                                         <i class="mr-3 fa fa-trash fs-18 text-danger cursor"></i> 
                                                    </a>
                                                    <a :href="'/admin/post/edit/1'+post._id">
                                                        <i class="ml-3 fa fa-pencil-square-o fs-18 text-success cursor"></i>
                                                    </a>
                                            </div>
                                            {{-- <a :href="'/admin/post/edit/1'+post._id"><span class="fa fa-pencil"> </span></a>  --}}
                                            {{-- <a v-on:click="deletePost(post._id)"><span class="fa fa-trash"> </span></a>  --}}
                                        </td>
                                    </tr>

                                    {{-- <tr >  
                                        <td >Jacob</td>
                                        <td class="text-center">Thornton</td>
                                        <td class="text-center">Thornton</td>
                                        <td >@fat</td>
                                        <td class="text-center">
                                                <span class="fa fa-pencil-square-o fs-20 text-primary"> </span> 
                                            </td>
                                        </tr>
                                        <tr>
                                            
                                            <td>Larry</td>
                                            <td class="text-center">Larry</td>
                                            <td class="text-center">the Bird</td>
                                            <td>@twitter</td>
                                            <td class="text-center">
                                                
                                                <span class="fa fa-pencil-square-o fs-20 text-primary"> </span> 
                                        </td>
                                    </tr> --}}
                                </tbody>
                            </table>

                        </div>

                        {{-- <div id="tab-2" class="tab-pane">
                            <strong>Published</strong>

                            <p>Thousand unknown plants are noticed by me: when I hear the buzz of the little
                                world among the stalks, and grow familiar with the countless indescribable
                                forms of the insects and flies, then I feel the presence of the Almighty,
                                who formed us in his own image, and the breath </p>

                            <p>I am alone, and feel the charm of existence in this spot, which was created
                                for the bliss of souls like mine. I am so happy, my dear friend, so absorbed
                                in the exquisite sense of mere tranquil existence, that I neglect my
                                talents. I should be incapable of drawing a single stroke at the present
                                moment; and yet.</p>

                        </div>

                        <div id="tab-3" class="tab-pane">
                                <strong>Draft</strong>

                                <p>Thousand unknown plants are noticed by me: when I hear the buzz of the little
                                    world among the stalks, and grow familiar with the countless indescribable
                                    forms of the insects and flies, then I feel the presence of the Almighty,
                                    who formed us in his own image, and the breath </p>

                                <p>I am alone, and feel the charm of existence in this spot, which was created
                                    for the bliss of souls like mine. I am so happy, my dear friend, so absorbed
                                    in the exquisite sense of mere tranquil existence, that I neglect my
                                    talents. I should be incapable of drawing a single stroke at the present
                                    moment; and yet.</p>

                            </div>
                    </div> --}}

                </div>
            </div>
            
        </div>
    </div>
    
</div>
</div>   
            <div v-if="loader">
                    <div id="loading"></div>
            </div>
@endsection
@section('scripts')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script src="{{asset('/admin/js/lfm.js')}}"></script>
<script>
    $('#lfm').filemanager();
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.min.js" integrity="sha256-chlNFSVx3TdcQ2Xlw7SvnbLAavAQLO0Y/LBiWX04viY=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.js" integrity="sha256-65r7bET363amdMrrizpE4H+N9OWh/aDrLWq2UWoqxlg=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@include('admin.scripts.viewposts')
@endsection