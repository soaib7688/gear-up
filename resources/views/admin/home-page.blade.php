@extends('admin.layouts.app')
@section('content')
<div class=" animated fadeInRight">
    <div class="row">
            @if (Session::has('flash_message'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! Session('flash_message') !!}</strong>
            </div>
            @endif
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif  
        <div class="col-lg-12">
            <div class="ibox-title">
                <h5 class="mt-2">Home Page</h5>
                

            </div>

            <div class="ibox-content">
                <div class="row">

                    <div class="col-xs-8">


                    {{-- <div class="mb-5 bg-danger p-2">
                        <h3 class="">Note: </h3>
                        <ol class="dd-list">
                           <li class="bg-default p-1"> - Double enter will be targeting the next section. Becareful !</li> 
                        </ol>
                    </div> --}}
                    
                    <h2>Block 1</h2>
                    <form action="{{url('/admin/pages/home-page')}}" method="POST">
                        @csrf
                        <input type="hidden" name="block" value="1">
                        <div class="form-group">
                            @php
                                $block1 = $content->block1;
                                $data = explode('<br>',$block1);
                            @endphp
                            <label for="">Heading</label>
                            <input type="text" name="heading" class="form-control" value="{{$data[0]}}">
                            <label for="">Content</label>
                            <textarea name="content" class="form-control" id="" cols="10" rows="5">{{$data[1]}}</textarea>
                        </div>

                        <div class="ibox-tools">
                            <input type="submit" class="btn btn-primary" value="Save">
                        </div>
                    </form>
<br><br>

                    <h2>Block 2</h2>
                    <form action="{{url('/admin/pages/home-page?block=2')}}" method="POST">
                        @csrf
                        <div class="form-group">
                                @php
                                    $block2 = $content->block2;
                                    $data = explode('<br>',$block2);
                                @endphp
                            <label for="">Heading</label>
                            <input type="text" name="heading" class="form-control" value="{{$data[0]}}">
                            <label for="">Content</label>
                            <textarea name="content" class="form-control" id="" cols="10" rows="5">{{$data[1]}}</textarea>
                        </div>

                        <div class="ibox-tools">
                            <input type="submit" class="btn btn-primary" value="Save">
                        </div>
                    </form>

                    <br><br>

                    <h2>Block 3</h2>
                    <form action="{{url('/admin/pages/home-page?block=3')}}" method="POST">
                        @csrf
                        <div class="form-group">
                                @php
                                    $block3 = $content->block3;
                                    $data = explode('<br>',$block3);
                                @endphp
                            <label for="">Heading</label>
                            <input type="text" name="heading" class="form-control" value="{{$data[0]}}">
                            <label for="">Content</label>
                            <textarea name="content" class="form-control" id="" cols="10" rows="5">{{$data[1]}}</textarea>
                        </div>

                        <div class="ibox-tools">
                            <input type="submit" class="btn btn-primary" value="Save">
                        </div>
                    </form>

                    <br><br>

                    <h2>Block 4</h2>
                    <form action="{{url('/admin/pages/home-page?block=4')}}" method="POST">
                        @csrf
                        <div class="form-group">
                                @php
                                    $block4 = $content->block4;
                                    $data = explode('<br>',$block4);
                                @endphp
                            <label for="">Heading</label>
                            <input type="text" name="heading" class="form-control" value="{{$data[0]}}">
                            <label for="">Content</label>
                            <textarea name="content" class="form-control" id="" cols="10" rows="5">{{$data[1]}}</textarea>
                        </div>

                        <div class="ibox-tools">
                            <input type="submit" class="btn btn-primary" value="Save">
                        </div>
                    </form>

                    <br><br>

                    <h2>Block 5</h2>
                    <form action="{{url('/admin/pages/home-page?block=5')}}" method="POST">
                        @csrf
                        <div class="form-group">
                                @php
                                    $block5 = $content->block5;
                                    $data = explode('<br>',$block5);
                                @endphp
                            <label for="">Heading</label>
                            <input type="text" name="heading" class="form-control" value="{{$data[0]}}">
                            <label for="">Content</label>
                            <textarea name="content" class="form-control" id="" cols="10" rows="5">{{$data[1]}}</textarea>
                        </div>

                        <div class="ibox-tools">
                            <input type="submit" class="btn btn-primary" value="Save">
                        </div>
                    </form>

                    <br><br>

                    <h2>Block 6</h2>
                    <form action="{{url('/admin/pages/home-page?block=6')}}" method="POST">
                        @csrf
                        <div class="form-group">
                                @php
                                    $block6 = $content->block6;
                                    $data = explode('<br>',$block6);
                                @endphp
                            <label for="">Heading</label>
                            <input type="text" name="heading" class="form-control" value="{{$data[0]}}">
                            <label for="">Content</label>
                            <textarea name="content" class="form-control" id="" cols="10" rows="5">{{$data[1]}}</textarea>
                        </div>

                        <div class="ibox-tools">
                            <input type="submit" class="btn btn-primary" value="Save">
                        </div>
                    </form>

                    <br><br>

                    <h2>Block 7</h2>
                    <form action="{{url('/admin/pages/home-page?block=7')}}" method="POST">
                        @csrf
                        <div class="form-group">
                                @php
                                    $block7 = $content->block7;
                                    $data = explode('<br>',$block7);
                                @endphp
                            <label for="">Heading</label>
                            <input type="text" name="heading" class="form-control" value="{{$data[0]}}">
                            <label for="">Content</label>
                            <textarea name="content" class="form-control" id="" cols="10" rows="5">{{$data[1]}}</textarea>
                        </div>

                        <div class="ibox-tools">
                            <input type="submit" class="btn btn-primary" value="Save">
                        </div>
                    </form>

                    <br><br>

                    <h2>Block 8</h2>
                    <form action="{{url('/admin/pages/home-page?block=8')}}" method="POST">
                        @csrf
                        <div class="form-group">
                                @php
                                    $block8 = $content->block8;
                                    $data = explode('<br>',$block8);
                                @endphp
                            <label for="">Heading</label>
                            <input type="text" name="heading" class="form-control" value="{{$data[0]}}">
                            <label for="">Content</label>
                            <textarea name="content" class="form-control" id="" cols="10" rows="5">{{$data[1]}}</textarea>
                        </div>

                        <div class="ibox-tools">
                            <input type="submit" class="btn btn-primary" value="Save" style="margin-bottom:50px">
                        </div>
                    </form>

                    

                    </div>


                </div>

            </div>
        </div>
    </div>

</div>
@endsection

@section('scripts')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.min.js"
    integrity="sha256-chlNFSVx3TdcQ2Xlw7SvnbLAavAQLO0Y/LBiWX04viY=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.js"
    integrity="sha256-65r7bET363amdMrrizpE4H+N9OWh/aDrLWq2UWoqxlg=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@include('admin.scripts.pagesScript')
@endsection