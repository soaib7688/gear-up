<nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                                <img alt="image" class="img-circle" src="img/profile_small.jpg" />
                            </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">David
                                            Williams</strong>
                                    </span> <span class="text-muted text-xs block">Art Director <b
                                            class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInDown m-t-xs">
                                <li><a href="profile.html">Profile</a></li>
                                <li><a href="login.html">Logout</a></li>
                            </ul>
                        </div>


                        <div class="logo-element">
                            F_S
                        </div>

                    </li>

                    <li class="active">
                        <a href="index.php">
                            <i class="fa fa-th-large"></i>
                            <span class="nav-label">Dashboards</span>
                        </a>
                    </li>


                    <li>
                        <a>
                            <i class="fa fa-thumb-tack"></i>
                            <span class="nav-label">Posts</span>
                            <span class="fa arrow"></span>
                        </a>

                        <ul class="nav nav-second-level">
                            <li><a href="./add_Post.php">Add Post</a></li>
                            <li><a href="./view_post.php">View Post</a></li>
                        </ul>
                    </li>

                    <li >
                        <a href="./catagory.php">
                            <i class="fa fa-user"></i>
                            <span class="nav-label">Catagory</span>
                        </a>

                    </li>

                    <li>
                        <a href="./author.php">
                            <i class="fa fa-user"></i>
                            <span class="nav-label">Author</span>
                        </a>

                    </li>


                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i
                            class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" method="post" action="search_results.html">
                        <div class="form-group">
                            <input type="text" placeholder="Search for something..." class="form-control"
                                name="top-search" id="top-search">
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="login.html">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                    </li>
                </ul>

            </nav>
        </div>