<div class="footer">
           
            <div class="text-center">
                <strong>Copyright</strong> <i>Friendly Solutioners</i> &copy; 2019
            </div>
        </div>
    </div>
</div>

 

<!-- Mainly scripts -->
 
 <script src="js/jquery-2.1.1.js"></script>
 <script src="js/bootstrap.min.js"></script>

 <script src="js/jquery.metisMenu.js"></script>

 <!-- Custom and plugin javascript !important javscript code -->
 <script src="js/friendlySolutioners.js"></script>

</body>

</html>