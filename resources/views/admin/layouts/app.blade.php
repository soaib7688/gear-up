<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> F_S | DashBoard</title>

    <link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/toastr.css')}}" rel="stylesheet">
    <style>
            #loading {
            background: url('giphy.gif') no-repeat center center;
            background-color: white;
            position: absolute;
            mix-blend-mode: multiply;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            z-index: 9999999;
        }
        </style>

</head>


<body class="pace-done">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="nav-header animated fadeInDown">
                        <div class="dropdown profile-element ">
                            <span >
                                <img alt="image" class="img-circle" src="{{asset('admin/images/profile.JPG')}}" height="56px" width="50px"/>
                            </span>

                            <a href="{{url('/admin/dashboard')}}" class="dropdown-toggle " >
                                <span class="clear"> <span class="block m-t-xs"> 
                                    <strong class="font-bold">
                                        Farrukh Rajput
                                    </strong>
                                </span>
                                
                                <span class="text-muted text-xs block">Web Developer </span>
                                </span> 
                            </a>

                            <ul class="dropdown-menu animated fadeInDown m-t-xs">
                                {{-- <li><a href="index.php">Profile</a></li> --}}
                                {{-- <li><a href="login.php">Logout</a></li> --}}
                            </ul>

                        </div>


                        <div class="logo-element">
                            F_S
                        </div>

                    </li>

                    <li class="{{Request::segment(2)=='dashboard'?'active':''}}">
                        <a href="{{url('/admin/dashboard')}}">
                            <i class="fa fa-th-large"></i>
                            <span class="nav-label">Dashboards</span>
                        </a>
                    </li>


                    <li class="{{ Request::segment(2)=='add-post'|| Request::segment(2)=='view-posts' ? 'active' : '' }}">
                        <a>
                            <i class="fa fa-thumb-tack"></i>
                            <span class="nav-label">Posts</span>
                            <!-- <span class="fa arrow"></span> -->
                        </a>

                        <ul class="nav nav-second-level">
                            <li class="{{ Request::segment(2)=='add-post' ? 'active' : '' }}"><a href="{{url('/admin/add-post')}}">Add Post</a></li>
                            <li class="{{Request::segment(2)=='view-posts'?'active':''}}"><a href="{{url('/admin/view-posts')}}">View Post</a></li>
                        </ul>
                    </li>

                    <li class="{{Request::segment(2)=='category'?'active':''}}">
                        <a href="{{url('/admin/category')}}">
                            <i class="fa fa-tags"></i>
                            <span class="nav-label">Catagory</span>
                        </a>

                    </li>

                    <li class="{{Request::segment(2)=='author'?'active':''}}">
                        <a href="{{url('/admin/author')}}">
                            <i class="fa fa-user"></i>
                            <span class="nav-label">Author</span>
                        </a>

                    </li>

                    <li class="{{Request::segment(3)=='home-page' || Request::segment(2)=='team' ?'active':''}}">
                        <a>
                            <i class="fa fa-file-text-o"></i>
                            <span class="nav-label">Main Pages</span>
                        </a>

                        <ul class="nav nav-second-level">
                            <li class="{{Request::segment(3)=='home-page'?'active':''}}"><a href="/admin/pages/home-page">Home Page</a></li>
                            <li class="{{Request::segment(2)=='team'?'active':''}}"><a href="{{url('/admin/team/create')}}">Our Team</a></li>
                        </ul>

                    </li>

                    <li class="{{Request::segment(2)=='script-setting'?'active':''}}">
                        <a href="/admin/script-setting">
                            <i class="fa fa-file-text-o"></i>
                            <span class="nav-label">Script Setting</span>
                        </a>

                    </li>

                    <li class="{{Request::segment(3)=='faq-page'||Request::segment(3)=='faq-category-page'?'active':''}}">
                            <a>
                                <i class="fa fa-file-text-o"></i>
                                <span class="nav-label">FAQ Page</span>
                            </a>
    
                            <ul class="nav nav-second-level">
                                <li class="{{Request::segment(3)=='faq-page'?'active':''}}"><a href="/admin/pages/faq-page">FAQs Page</a></li>
                                <li class="{{Request::segment(3)=='faq-category-page'?'active':''}}"><a href="/admin/pages/faq-category-page">FAQs Category</a></li>
                            </ul>
    
                        </li>


                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                        <i class="fa fa-bars"></i> 
                    </a>
                    <form role="search" class="navbar-form-custom" method="post" action="search_results.html">
                        <div class="form-group">
                            <input type="text" placeholder="Search for something..." class="form-control"
                                name="top-search" id="top-search">
                        </div>
                    </form>

                    <div class="navbar-right">
                    </div>

                </div>
                <ul class="nav navbar-top-links pull-right">
                    <li>
                        <a href="login.php" >
                            <i class="fa fa-sign-out text-danger"></i>
                             <span class="text-danger">Log out</span> 
                        </a>
                    </li>
                </ul>

            </nav>
        </div>

        <!-- page-wrapper top Menu ends here -->
        @yield('content')
        
<!-- footer -->

<div class="footer">
           
            <div class="text-center">
                <strong>Copyright</strong> <i>Friendly Solutioners</i> &copy; 2019
            </div>
        </div>
    </div>
</div>

 

<!-- Mainly scripts -->
 
 <script src="{{asset('admin/js/jquery-2.1.1.js')}}"></script>
 <script src="{{asset('admin/js/jquery-ui-1.10.4.min.js')}}"></script>
 <script src="{{asset('admin/js/bootstrap.min.js')}}"></script>

 <script src="{{asset('admin/js/jquery.metisMenu.js')}}"></script>

 <!-- Custom and plugin javascript !important javscript code -->
 <script src="{{asset('admin/js/friendlySolutioners.js')}}"></script>
 @yield('scripts')

</body>

</html>