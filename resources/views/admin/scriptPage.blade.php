@extends('admin.layouts.app')
@section('content')
<div class="row wrapper wrapper-content-fs animated fadeInRight">
    <div id="VueJs">
        <div class="row">
            @if (Session::has('flash_message'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! Session('flash_message') !!}</strong>
            </div>
            @endif
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif  
            <div class="col-lg-12">
                <div class="ibox float-e-margins border-bottom">
                    <div class="ibox-title pt-1">
                        <div class="d-flex flex-between ">
                            <h3>
                                Script Settings
                            </h3>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form action="/admin/saveScript" enctype="multipart/form-data" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="">Paste Header Script Here</label>
                                <textarea class="form-control resize-none" type="text" rows="10" cols="50" name="script">{{@$data->script}}</textarea>
                            </div>

                            <div class="d-flex flex-end">

                                <button class="btn btn-md btn-primary m-0">
                                    <i class="fa fa-paper-plane-o"></i> Save
                                </button>
                            </div>

                        </form>
                    </div>

                    <div class="ibox-content">
                        <form action="/admin/saveBodyScript" enctype="multipart/form-data" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="">Paste Body Script Here</label>
                                <textarea class="form-control resize-none" type="text" rows="10" cols="50" name="bodyscript">{{@$data->body_script}}</textarea>
                            </div>

                            <div class="d-flex flex-end">

                                <button class="btn btn-md btn-primary m-0">
                                    <i class="fa fa-paper-plane-o"></i> Save
                                </button>
                            </div>

                        </form>
                    </div>

                    <div class="ibox-content">
                        <form action="/admin/saveFooterScript" enctype="multipart/form-data" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="">Paste Footer Script Here</label>
                                <textarea class="form-control resize-none" type="text" rows="10" cols="50" name="footerscript">{{@$data->footer_script}}</textarea>
                            </div>

                            <div class="d-flex flex-end">

                                <button class="btn btn-md btn-primary m-0">
                                    <i class="fa fa-paper-plane-o"></i> Save
                                </button>
                            </div>

                        </form>
                    </div>


                    

                </div>
            </div>

        </div>
    </div>
</div>
@endsection