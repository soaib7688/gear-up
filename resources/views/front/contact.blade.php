@extends('front.layouts.app')
@section('class')class="contact"@endsection
@section('content')
<header class="site-header">
    <div class="container">
        <div class="row justify-content-between align-items-center">
            <div class="col-6 col-md-3">
                <a href="{{url('/')}}" class="logo-link">
                    <img src="{{asset('front/images/logo.svg')}}">
                </a>
            </div>
            <div class="col-6 text-right d-md-none">
                <a href="#" class="menu-toggle d-inline-flex flex-column align-items-end justify-content-between">
                    <span class="line small"></span>
                    <span class="line"></span>
                </a>
            </div>
            <div class="col-md-9 text-right main-menu-container">
                <a href="#"
                    class="d-md-none menu-toggle d-inline-flex flex-column align-items-end justify-content-between">
                    <i class="fas fa-times"></i>
                </a>
                <ul class="list-unstyled mb-0 header-menu">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li><a href="{{url('/team')}}">Our Team</a></li>
                    <li><a href="{{url('/blog')}}">Blog</a></li>
                    <li><a href="{{url('/faqs')}}">FAQ</a></li>
                    <li class="active"><a href="{{url('/contact')}}">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>
<section class="contact-main">
    <div id="form-recponce" class="alert form-error alert-dismissible wow fadeIn show" role="alert">
        <h3 class="mb-0">THANK YOU!</h3>
        <p id="form-sub-msg" class="m-0">Your message has been successfuly sent to GearUp!</p>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="fas fa-times"></i>
        </button>
    </div>
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-12 col-md-7 pl-md-2">
				<h2 class="mb-4">DROP US A LINE!</h2>
				<div id="errors"></div>
                <div class="contact-form">
                    <form action="" id="contact-form-ajax">
                        <div class="form-group">
                            <input type="text" class="form-control form-text pl-4" id="name" name="name"
                                placeholder="Full Name" required>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control form-text pl-4" id="email" name="email"
                                placeholder="Email Address" required="">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control form-text pl-4" id="message" name="message" placeholder="Message"
                                rows="3" required=""></textarea>
                        </div>
                        <button type="submit" id="submit_btn" class="btn site-btn">SUBMIT</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<footer class="site-footer">
	<div class="container">
		<div class="row justify-content-md-between">
			<div class="col-12 col-md-auto my-auto">
				<div class="footer-col-in text-center text-md-left">
					<img class="mb-2" src="{{asset('front/images/logo-white.svg')}}">
					<p class="mb-0"><a class="text-white" href="mailto:info@gearupcamera.com">info@gearupcamera.com</a></p>
				</div>
			</div>
			<div class="col-12 col-md-auto px-0"><span class="divider-footer"></span></div>
			<div class="col-6 col-md-auto">
				<div class="footer-col-in">
					<h6>Company</h6>
					<ul class="list-unstyled mb-0">
						<li><a href="#">Our Team</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="#">Contact Us</a></li>
					</ul>
				</div>
			</div>
			<div class="col-6 col-md-auto">
				<div class="footer-col-in">
					<h6>Support</h6>
					<ul class="list-unstyled mb-0">
						<li><a href="#">Help Center</a></li>
						<li><a href="#">Terms of Service</a></li>
						<li><a href="#">Privacy Policy</a></li>
					</ul>
				</div>
			</div>
			<div class="col-12 col-md-auto">
				<div class="footer-col-in footer-col-social">
					<h6>Community</h6>
					<ul class="list-unstyled mb-0 footer-social">
						<li><a href="https://www.facebook.com/gearuptheapp/" target="_blank"><i class="fab fa-facebook-f"></i><span>Facebook</span></a></li>
						<li><a href="https://www.instagram.com/gearup.camera/" target="_blank"><i class="fab fa-instagram"></i><span>Instagram</span></a></li>
					</ul>
				</div>
			</div>
			<div class="col-12 col-md-auto mt-auto">
				<div class="footer-col-in text-center text-md-left">
					<p class="mb-0 copy-right">© 2019 GearUp LLC. All Rights reserved.</p>
				</div>
			</div>
		</div>
	</div>
</footer>


<script>
    $("#contact-form-ajax").submit(function (event) {

        var data = $(this).closest("form").serializeArray();
        $.ajax({
            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            },
            type: "post",
            global: "false",
            datatype: "html",
            url: "/contactususers",
            data: data,
        }).done(function (d) {
			document.getElementById("contact-form-ajax").reset();
			$('#form-recponce').addClass('alert-success active');

        }).fail(function (errors) {
            var errorsHtml = '';
            var $div = $("<p>", {
                id: "foo",
                "class": "a"
            });
            let error = errors.responseJSON['errors'];
            $.each(error, function (i, val) {
                errorsHtml += '<ul><li>' + val + '</li></ul>';
            });
            console.log(errorsHtml);
            $('#errors').html(errorsHtml);
            return false;
        });

        event.preventDefault();


    });
</script>
@endsection
