<div class="container">
<header class="site-header">
            <div class="row justify-content-between align-items-center">
                <div class="col-6 col-md-3">
                    <a href="index.html" class="logo-link">
                        <img src="{{asset('front/images/logo/logo.svg')}}">
                    </a>
                </div>
                <div class="col-6 text-right d-md-none">
                    <a href="#" class="menu-toggle d-inline-flex flex-column align-items-end justify-content-between">
                        <span class="line small"></span>
                        <span class="line"></span>
                    </a>
                </div>
                <div class="col-md-9 text-right main-menu-container">
                    <a href="#" class="d-md-none menu-toggle d-inline-flex flex-column align-items-end justify-content-between">
                        <i class="fas fa-times"></i>
                    </a>
                    <ul class="list-unstyled mb-0 header-menu">
                        <li class="active"><a href="{{url('/')}}">Home</a></li>
                        <li><a href="{{url('/team')}}">Our Team</a></li>
                        <li><a href="{{url('/blog')}}">Blog</a></li>
                        <li><a href="{{url('/contact')}}">Contact Us</a></li>
                        <li><a href="{{url('/faqs')}}">FAQ</a></li>
                    </ul>
                </div>
            </div>
        </header>
    </div>