<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>GearUp / Buy and sell camera gearupcamera</title>
    <link rel="shortcut icon" type="image/x-icon" href="http://gearupcamera.com/front/images/favicon.png"/>
    <link rel="canonical" href="http://gearupcamera.com"/>
    <meta property="og:site_name" content="GearUp / Buy and sell camera gear"/>
    <meta property="og:title" content="GearUp / Buy and sell camera gear"/>
    <meta property="og:url" content="http://gearupcamera.com"/>
    <meta property="og:type" content="website"/>
    <meta property="og:description" content="GearUp is where photographers and filmmakers buy and sell their camera gear. Find great deals on new and used camera equipment, or list your gear for free in seconds."/>
    <meta property="og:image" content="http://gearupcamera.com/front/images/gearshareicon.png"/>
    <meta property="og:image:width" content="500"/>
	<meta property="og:image:height" content="187"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('front/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/animate.min.css')}}">
	<link rel="stylesheet" href="{{asset('front/css/style.css')}}">
	<link rel="stylesheet" href="{{asset('front/css/toastr.css')}}">
    <link href="{{asset('front/css/horizontal-slim-10_7.css')}}" rel="stylesheet" type="text/css">
    <script src="{{asset('front/js/jquery.min.js')}}"></script>
	<script src="{{asset('front/js/popper.min.js')}}"></script>
	<script src="{{asset('front/js/vue.min.js')}}"></script>
	<script src="{{asset('front/js/vue-resource.js')}}"></script>
    <script src="{{asset('front/js/wow.min.js')}}"></script>
    <script src="{{asset('front/js/bootstrap.min.js')}}"></script>
</head>
<body @yield('class')>

<section id="main">
<!-- Header -->


@yield('content')

{{-- <footer class="site-footer">
	<div class="container">
		<div class="row justify-content-md-between">
			<div class="col-12 col-md-auto my-auto">
				<div class="footer-col-in text-center text-md-left">
					<img class="mb-2" src="{{asset('front/images/logo-white.svg')}}">
					<p class="mb-0"><a class="text-white" href="mailto:info@gearupcamera.com">info@gearupcamera.com</a></p>
				</div>
			</div>
			<div class="col-12 col-md-auto px-0"><span class="divider-footer"></span></div>
			<div class="col-6 col-md-auto">
				<div class="footer-col-in">
					<h6>Company</h6>
					<ul class="list-unstyled mb-0">
						<li><a href="#">Our Team</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="#">Contact Us</a></li>
					</ul>
				</div>
			</div>
			<div class="col-6 col-md-auto">
				<div class="footer-col-in">
					<h6>Support</h6>
					<ul class="list-unstyled mb-0">
						<li><a href="#">Help Center</a></li>
						<li><a href="#">Terms of Service</a></li>
						<li><a href="#">Privacy Policy</a></li>
					</ul>
				</div>
			</div>
			<div class="col-12 col-md-auto">
				<div class="footer-col-in footer-col-social">
					<h6>Community</h6>
					<ul class="list-unstyled mb-0 footer-social">
						<li><a href="https://www.facebook.com/gearuptheapp/" target="_blank"><i class="fab fa-facebook-f"></i><span>Facebook</span></a></li>
						<li><a href="https://www.instagram.com/gearup.camera/" target="_blank"><i class="fab fa-instagram"></i><span>Instagram</span></a></li>
					</ul>
				</div>
			</div>
			<div class="col-12 col-md-auto mt-auto">
				<div class="footer-col-in text-center text-md-left">
					<p class="mb-0 copy-right">© 2019 GearUp LLC. All Rights reserved.</p>
				</div>
			</div>
		</div>
	</div>
</footer> --}}

</section>

<script defer src="https://use.fontawesome.com/releases/v5.7.1/js/all.js" integrity="sha384-eVEQC9zshBn0rFj4+TU78eNA19HMNigMviK/PU/FFjLXqa/GKPgX58rvt5Z8PLs7" crossorigin="anonymous"></script>
<script src="{{asset('front/js/script.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
</body>
</html>