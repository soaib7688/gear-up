@extends('front.layouts.app')
@section('class')class="home"@endsection
@include('front.layouts.header')
@section('content')
<section class="home-hero">
		<div class="container">
			<div class="row align-items-center justify-content-between">
				<div class="col-12 col-md-8 mb-5 mb-md-0 wow slideInLeft">
					<div class="hero-content-box text-center text-md-left ">
						<h1>JOIN THE COMMUNITY OF IMAGE MAKERS</h1>
						<h5>GearUp is the #1 marketplace and community for photographers and filmmakers.</h5>
					</div>
	
					<!-- Begin Mailchimp Signup Form -->
					<div class="subscribe-form-box">
						<div id="mc_embed_signup">
							<form action="https://gearupcamera.us3.list-manage.com/subscribe/post?u=986cb4a1837141fb6922b52e3&amp;id=bd3efcebdd" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
								<div id="mc_embed_signup_scroll" class="input-group">
									<input type="email" class="form-control form-text" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Email me when the app launches!" required>
									<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
									<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_986cb4a1837141fb6922b52e3_bd3efcebdd" tabindex="-1" value=""></div>
									<div class="input-group-append">
										<input class="btn form-btn" type="submit" value="Submit" name="subscribe" id="mc-embedded-subscribe" class="button">
									</div>
								</div>
							</form>
						</div>
						<!--End mc_embed_signup-->
						<div class="error_msg" id="error_msg" style="display: none;">
							<p>Invalid Email Address</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4 col-xl-4 text-center wow slideInRight">
					<div class="hero-img-in">
						<img src="{{asset('front/images/Hero_image.png')}}">
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="gearup-home">
		<div class="container">
			<div class="row justify-content-between align-items-center">
				<div class="col-12 col-md-7 col-lg-6 order-md-1 wow fadeInUp">
					<div class="gearup-box">
						<h2>Get good money for your camera gear</h2>
						<div class="gearup-box-text">
							<p>GearUp has the lowest transaction fees around helping you get the most for your gear.</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4 text-center img-bg-layer wow fadeInUp">
					<div class="gear-up-img">
						<img src="{{asset('front/images/mysale.png')}}">
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="gearup-home">
		<div class="container">
			<div class="row justify-content-between align-items-center">
				<div class="col-12 col-md-7 col-lg-6 wow fadeInUp">
					<div class="gearup-box">
						<h2>Join the passionate community</h2>
						<div class="gearup-box-text">
							<p>Our community of photographers and filmmakers care and respect one another.</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4 text-center img-bg-layer img-bg-layer-right wow fadeInUp">
					<div class="gear-up-img">
						<img src="{{asset('front/images/chat.png')}}">
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="gearup-home mb-md-5">
		<div class="container">
			<div class="row justify-content-between align-items-center">
				<div class="col-12 col-md-7 col-lg-6 order-md-1 wow fadeInUp">
					<div class="gearup-box subscribe-form-cont">
						<h2>Feel confident using our secure platform</h2>
						<div class="gearup-box-text">
							<p>Our sellers are verified and GearUp has your back with each transaction.</p>
						</div>
						<div class="subscribe-form-box subscribe-form-bottom">
							<!-- Begin Mailchimp Signup Form -->
							<div id="mc_embed_signup">
								<form action="https://gearupcamera.us3.list-manage.com/subscribe/post?u=986cb4a1837141fb6922b52e3&amp;id=bd3efcebdd" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
									<div id="mc_embed_signup_scroll" class="input-group">
										<input type="email" class="form-control form-text" value="" name="EMAIL" class="email" id="mce-EMAIL-two" placeholder="Email me when the app launches!" required>
										<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
										<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_986cb4a1837141fb6922b52e3_bd3efcebdd" tabindex="-1" value=""></div>
										<div class="input-group-append">
											<input class="btn form-btn" type="submit" value="Submit" name="subscribe" id="mc-embedded-subscribe_two" class="button">
										</div>
									</div>
								</form>
							</div>
							<!--End mc_embed_signup-->
							<div class="error_msg" id="error_msg-two" style="display: none;">
								<p>Invalid Email Address</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4 col-lg-4 text-center img-bg-layer wow fadeInUp">
					<div class="gear-up-img">
						<img src="{{asset('front/images/varification.png')}}">
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection