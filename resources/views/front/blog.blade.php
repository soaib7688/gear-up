@extends('front.layouts.app')
@section('class')class="home"@endsection

@section('content')



<div id="VueJs" class="blog-container">
    {{-- <div class="pre-loader" v-if="loader">
        <img src="{{asset('front/images/loader.gif')}}" alt="">
</div> --}}

<nav class="blog-nav">
    <div class="container">
        <div class="row justify-content-between align-items-center text-secondary">
            <div class="col-6 col-md-3">
                <a href="{{url('/')}}" class="logo-link">
                    <img src="{{asset('front/images/logo.svg')}}">
                </a>
            </div>
            <div class="col-6 text-right d-md-none">
                <a href="#" class="menu-toggle d-inline-flex flex-column align-items-end justify-content-between">
                    <span class="line small"></span>
                    <span class="line"></span>
                </a>
            </div>
            <div class="col-md-9 text-right main-menu-container">
                <a href="#"
                    class="d-md-none menu-toggle d-inline-flex flex-column align-items-end justify-content-between">
                    <i class="fas fa-times"></i>
                </a>
                <ul class="list-unstyled mb-0 header-menu blog-menu">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li><a href="{{url('/team')}}">Our Team</a></li>
                    <li class="active"><a href="{{url('/blog')}}">Blog</a></li>
                    <li><a href="{{url('/faqs')}}">FAQ</a></li>
                    <li><a href="{{url('/contact')}}">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>


<div class="container pb-sm-4 p-0">

    <div class="banner-wrapper" v-for="(post,index) in posts" v-if="index<1"
        v-bind:style='{ backgroundImage: "url(" + post.cover_img + ")", }'>
        <div class="banner-title-block d-none d-lg-block">

            <div class="banner-post-details">
                <span class="text-capitalize">@{{setDate(post.created_at)}}</span>
                <span class="dark-color"> // </span>
                <span class="text-capitalize"> @{{post.category[0].name}}</span>
            </div>

            <div class="article-title">
                <h2 class="text-uppercase">@{{post.name.substring(0,40)+".."}} </h2>
            </div>

            <a :href="'/blog/'+post.slug+'-1'+post._id" class="banner-read-more">
                <div class="text-uppercase"> read article </div>
            </a>
        </div>
    </div>

    <!-- < Blog Filter Box> -->
    <div class="catagory-filter-box">
        <span class="catagory-filter-item filter-active" id="all" v-on:click="fetchPosts">all</span>
        {{-- <div v-for="(category,index) in categories" v-if="index < 4">
            <span> / </span>
            <span class="catagory-filter-item" :id="category._id"
                v-on:click="fetchByCategory(category._id)">@{{category.name}}</span>
        </div> --}}
    </div>

    <!-- </ Blog Filter Box -->
    <div class="container">
        <div class="row">

            <div class="col-md-6 col-lg-4 post-card " v-for="(post,index) in posts" v-if="index>0 && index < limit">
                <div class="post-card-catagory">
                    <span>@{{post.category[0].name}}</span>
                </div>
                <img class="img-fluid" :src="getUrl(post.cover_img)" alt="">
                <div class="post-card-detail ">
                    <div class="post-card-detail-date">@{{setDate(post.created_at)}}</div>
                    <a :href="'/blog/'+post.slug+'-1'+post._id">
                        <div class="post-card-detail-title">@{{post.name.substring(0,50)+".."}}</div>
                    </a>
                </div>
            </div>

            <div class="col-md-6 col-lg-4 p-4 mb-5" v-on:click="limit+=6" v-if="!isLast()">

                <div class="show-more py-5 px-3 d-none d-md-block">
                    <div class="py-5">
                        <div class="text-uppercase">Show</div>
                        <div class="text-uppercase ">more articles <span>&rarr; </span> </div>
                    </div>
                </div>

                <div class="d-flex justify-content-center d-md-none">
                    <button class="btn btn-default  btn-outline-secondary">
                        <h4 class="text-center mb-0">Show More Articles &rarr;</h4>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

<!-- Newsletter -->
<div class="container-fluid p-0">
    <div class="row newsletter-box">
        <div class="container">
            <div class="row m-0 py-5">
                <div class="col-lg-6 ">
                    <div class="newsletter-box-heading">join the community!</div>
                </div>
                <div class="col-lg-6 text-lg-right mb-5">
                    <div class="newsletter-box-text grey-text">Subscribe to our newsletter get free photo /vedio content, tips,
                        tricks and promotions.</div>
                </div>

                <div class="col-lg-8 mt-4 mt-lg-2">
                    <input type="text" class="newsletter-email" placeholder="your email">
                </div>
                <div class="col-lg-4 mt-lg-2 pr-4">
                    <button class="newsletter-btn">Subscribe</button>
                </div>
            </div>
        </div>
    </div>


</div>

<footer class="site-footer">
	<div class="container">
		<div class="row justify-content-md-between">
			<div class="col-12 col-md-auto my-auto">
				<div class="footer-col-in text-center text-md-left">
					<img class="mb-2" src="{{asset('front/images/logo-white.svg')}}">
					<p class="mb-0"><a class="text-white" href="mailto:info@gearupcamera.com">info@gearupcamera.com</a></p>
				</div>
			</div>
			<div class="col-12 col-md-auto px-0"><span class="divider-footer"></span></div>
			<div class="col-6 col-md-auto">
				<div class="footer-col-in">
					<h6>Company</h6>
					<ul class="list-unstyled mb-0">
						<li><a href="#">Our Team</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="#">Contact Us</a></li>
					</ul>
				</div>
			</div>
			<div class="col-6 col-md-auto">
				<div class="footer-col-in">
					<h6>Support</h6>
					<ul class="list-unstyled mb-0">
						<li><a href="#">Help Center</a></li>
						<li><a href="#">Terms of Service</a></li>
						<li><a href="#">Privacy Policy</a></li>
					</ul>
				</div>
			</div>
			<div class="col-12 col-md-auto">
				<div class="footer-col-in footer-col-social">
					<h6>Community</h6>
					<ul class="list-unstyled mb-0 footer-social">
						<li><a href="https://www.facebook.com/gearuptheapp/" target="_blank"><i class="fab fa-facebook-f"></i><span>Facebook</span></a></li>
						<li><a href="https://www.instagram.com/gearup.camera/" target="_blank"><i class="fab fa-instagram"></i><span>Instagram</span></a></li>
					</ul>
				</div>
			</div>
			<div class="col-12 col-md-auto mt-auto">
				<div class="footer-col-in text-center text-md-left">
					<p class="mb-0 copy-right">© 2019 GearUp LLC. All Rights reserved.</p>
				</div>
			</div>
		</div>
	</div>
</footer>

@include('front.scripts.blog')
@endsection
