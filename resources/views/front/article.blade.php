@extends('front.layouts.app')
@section('class')
class="home"
@endsection
@section('content')
<div id="VueJs">
<section id="main">
	<!-- Header -->
	<nav class="blog-nav">
        <div class="container">
            <div class="row justify-content-between align-items-center text-secondary">
                <div class="col-6 col-md-3">
                    <a href="{{url('/')}}" class="logo-link">
                        <img src="{{asset('front/images/logo.svg')}}">
                    </a>
                </div>
                <div class="col-6 text-right d-md-none">
                    <a href="#" class="menu-toggle d-inline-flex flex-column align-items-end justify-content-between">
                        <span class="line small"></span>
                        <span class="line"></span>
                    </a>
                </div>
                <div class="col-md-9 text-right main-menu-container">
                    <a href="#" class="d-md-none menu-toggle d-inline-flex flex-column align-items-end justify-content-between">
                        <i class="fas fa-times"></i>
                    </a>
                    <ul class="list-unstyled mb-0 header-menu blog-menu">
                        <li ><a href="{{url('/')}}">Home</a></li>
                        <li><a href="{{url('/team')}}">Our Team</a></li>
                        <li class="active"><a href="{{url('/blog')}}">Blog</a></li>
                        <li><a href="{{url('/faqs')}}">FAQ</a></li>
                        <li><a href="{{url('/contact')}}">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
	</nav>

	<div class="container">

		<div class="row flex-column align-items-center">
			<!-- < artical heading >  -->
			<div class="col-12 col-md-8  my-4">
				<div class="article-heading text-uppercase font-weight-bolder">{{$post->name}}</div>
				<div class="article-author-name d-inline-block mt-4">
					<p>by <span class="text-capitalize">{{$post->author->name}}</span></p>
				</div>

				<div class="my-4">
					{!!$post->content!!}
				</div>



				<h6 class="text-right my-4">
					{{$post->author->name}}.
				</h6>

				<!--<div class="article-social-icon-lg d-none d-lg-block">
					<a href="https://www.facebook.com/gearuptheapp/" title="Pinterest">
						<img src="{{asset('front/images/pinterest-icon.png')}}" alt="">
					</a>

					<a href="https://www.facebook.com/gearuptheapp/" title="Facebook">
						<img src="{{asset('front/images/facebook-icon.png')}}" alt="">
					</a>

					<a href="https://www.facebook.com/gearuptheapp/" title="Twitter">
						<img src="{{asset('front/images/twitter-icon.png')}}" alt="">
					</a>
				</div>-->

				<div class=" article-social-icon">
					<div class="w-50 d-flex justify-content-between m-auto">
						<a href="https://www.facebook.com/gearuptheapp/" title="Pinterest">
							<img src="{{asset('front/images/pinterest-icon.png')}}" alt="">
						</a>

						<a href="https://www.facebook.com/gearuptheapp/" title="Facebook">
							<img src="{{asset('front/images/facebook-icon.png')}}" alt="">
						</a>

						<a href="https://www.facebook.com/gearuptheapp/" title="Twitter">
							<img src="{{asset('front/images/twitter-icon.png')}}" alt="">
						</a>
					</div>
				</div>

				<div class="col-12 response-form">
					<textarea class="form-control" id="comment" cols="30" rows="5" placeholder="Write a response"></textarea>
					<input class="form-control" type="hidden" value="{{$post->_id}}" id="post_id">
					<input class="form-control" type="text" id="name" placeholder="Name">
					<input class="form-control" type="text" id="email" placeholder="Email">
					<div class="d-flex justify-content-end">
						<button class="btn btn-dark px-5 rounded-0 " v-on:click="postComment">Publish</button>
					</div>
				</div>

				<div class="my-5 ">
					<h6 class="text-capitalize"> <span>@{{comments.length}}</span> responses</h6>

					<div class="article-response p-3 my-3" v-for="comment in comments">
						<h6 class="article-respnse-user">@{{comment.name}}</h6>
						<div class="response-date">@{{setDate(comment.created_at)}}</div>
						<p class="article-comment">
							@{{comment.comment}}
						</p>
						{{-- <div class="d-flex justify-content-end">
							<button class="btn btn-outline-secondary rounded-0">Reply</button>

						</div> --}}
					</div>
				</div>



			</div>
		</div>

	</div>

</section>
</div>

<footer class="site-footer">
	<div class="container">
		<div class="row justify-content-md-between">
			<div class="col-12 col-md-auto my-auto">
				<div class="footer-col-in text-center text-md-left">
					<img class="mb-2" src="{{asset('front/images/logo-white.svg')}}">
					<p class="mb-0"><a class="text-white" href="mailto:info@gearupcamera.com">info@gearupcamera.com</a></p>
				</div>
			</div>
			<div class="col-12 col-md-auto px-0"><span class="divider-footer"></span></div>
			<div class="col-6 col-md-auto">
				<div class="footer-col-in">
					<h6>Company</h6>
					<ul class="list-unstyled mb-0">
						<li><a href="#">Our Team</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="#">Contact Us</a></li>
					</ul>
				</div>
			</div>
			<div class="col-6 col-md-auto">
				<div class="footer-col-in">
					<h6>Support</h6>
					<ul class="list-unstyled mb-0">
						<li><a href="#">Help Center</a></li>
						<li><a href="#">Terms of Service</a></li>
						<li><a href="#">Privacy Policy</a></li>
					</ul>
				</div>
			</div>
			<div class="col-12 col-md-auto">
				<div class="footer-col-in footer-col-social">
					<h6>Community</h6>
					<ul class="list-unstyled mb-0 footer-social">
						<li><a href="https://www.facebook.com/gearuptheapp/" target="_blank"><i class="fab fa-facebook-f"></i><span>Facebook</span></a></li>
						<li><a href="https://www.instagram.com/gearup.camera/" target="_blank"><i class="fab fa-instagram"></i><span>Instagram</span></a></li>
					</ul>
				</div>
			</div>
			<div class="col-12 col-md-auto mt-auto">
				<div class="footer-col-in text-center text-md-left">
					<p class="mb-0 copy-right">© 2019 GearUp LLC. All Rights reserved.</p>
				</div>
			</div>
		</div>
	</div>
</footer>

@include('front.scripts.article')
@endsection
{{--

	<script defer src="https://use.fontawesome.com/releases/v5.7.1/js/all.js"
		integrity="sha384-eVEQC9zshBn0rFj4+TU78eNA19HMNigMviK/PU/FFjLXqa/GKPgX58rvt5Z8PLs7" crossorigin="anonymous">
	</script>
	<script src="js/script.js"></script>
</body>

</html> --}}
