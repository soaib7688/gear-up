@extends('front.layouts.app')
@section('class')class="home"
@endsection
@section('content')


<div class="bg-image-box container wow slideInRight">
		<img  class="bg-image img-fluid " src="{{asset('front/images/home/top1.png')}}"  alt="">
	</div>

	<div class="container main-page-container">
		<header class="site-header">
			<div class="row align-items-center justify-content-between">
				<div class="col-6 col-sm-3 col-lg-3 col-xl-4">
					<a href="index.html" class="logo-link">
						<img src="{{asset('front/images/logo.svg')}}">
					</a>
				</div>
				<div class="col-6 col-sm-9 text-right d-md-none">
					<a href="#" class="menu-toggle d-inline-flex flex-column align-items-end justify-content-between">
						<span class="line small"></span>
						<span class="line"></span>
					</a>
				</div>
				<div class="col-md-9 col-xl-8 text-right main-menu-container">
					<a href="#" class="d-sm-none menu-toggle ">
						<i class="fas fa-times"></i>
					</a>
					<ul class="list-unstyled mb-0 header-menu">
						<li class="active"><a href="{{url('/')}}">Home</a></li>
						<li><a href="{{url('/team')}}">Our Team</a></li>
						<li><a href="{{url('/blog')}}">Blog</a></li>
						<li><a href="{{url('/faqs')}}">FAQ</a></li>
						<li><a href="{{url('/contact')}}">Contact Us</a></li>
					</ul>
				</div>
			</div>
		</header>
	</div>

	<section class="home-hero ">
		<div class="container ">
			<div class="row ">
				<div class="col-12 col-md-6 col-lg-6 wow slideInLeft m-auto m-md-0">
					<div class="hero-content-box">
						@php
							$block1 = $data['block1'];
							$bdata = explode('<br>',$block1);
						@endphp
						<h1 class="text-uppercase main-left-heading">{{$bdata[0]}}</h1>
						<h5 class="main-left-subheading">{{$bdata[1]}}</h5>
					</div>

					<div class="subscribe-form-box pb-0 pb-lg-5">
						<div id="mc_embed_signup">
							<form action="https://gearupcamera.us3.list-manage.com/subscribe/post?u=986cb4a1837141fb6922b52e3&amp;id=bd3efcebdd" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
								<div id="mc_embed_signup_scroll" class="input-group">
									<input type="email" class="form-control form-text m-0" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Email me when the app launches!" required>
									<div style="position: absolute; left: -5000px;" aria-hidden="true">
									<input type="text" name="b_986cb4a1837141fb6922b52e3_bd3efcebdd" tabindex="-1" value=""></div>
									<div class="input-group-append ml-lg-2 ml-md-2">
										<input class="btn form-btn" type="submit" value="Submit" name="subscribe" id="mc-embedded-subscribe" class="button">
									</div>
								</div>
							</form>
						</div>
						<!--End mc_embed_signup-->
						<div class="error_msg" id="error_msg" style="display: none;">
							<p>Invalid Email Address</p>
						</div>
					</div>
				</div>

				{{-- <div class="col-12 col-md-4 text-center  wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
					<div class="gear-up-img">
						<img class="" src="{{asset('front/images/home/top.png')}}">
					</div>
				</div> --}}
			</div>
		</div>
	</section>

<div class="bg-image-box mt-md-5 wow slideInLeft container">
	<img class="middle-mobile" src="{{asset('front/images/home/mockup - mobile@3x.png')}}">
	<img class="middle-tab " src="{{asset('front/images/home/mockup - tablet.png')}}">
	<img class="middle-wide img-fluid  d-none d-xl-block mt-5" src="{{asset('front/images/home/mockup - desktop@3x.png')}}">
</div>

<section class="gearup-home ">
	<div class="container ">
		<div class="row ">
			<div class="col-12 col-xl-6 order-1 wow fadeInUp middle-wide-text mt-xl-5" >

				<div class="gearup-box mb-4">
					@php
						$block2 = $data['block2'];
						$bdata = explode('<br>',$block2);
					@endphp
					<img class="img-fluid favicon my-3" src="{{asset('front/images/percent@10x.png')}}" alt="">
					<h2>{{$bdata[0]}}</h2>
					<div class="gearup-box-text">
						<p>{{$bdata[1]}}</p>
					</div>
				</div>


				<div class="gearup-box mb-4">
					@php
						$block3 = $data['block3'];
						$bdata = explode('<br>',$block3);
					@endphp
						<img class="img-fluid favicon my-3" src="{{asset('front/images/prize@10x.png')}}" alt="">
						<h2>{{$bdata[0]}}</h2>
						<div class="gearup-box-text">
							<p>{{$bdata[1]}}</p>
						</div>
					</div>

				<div class="gearup-box">
					@php
						$block4 = $data['block4'];
						$bdata = explode('<br>',$block4);
					@endphp
					<img class="img-fluid favicon my-3" src="{{asset('front/images/shield@10x.png')}}" alt="">
					<h2>{{$bdata[0]}}</h2>
					<div class="gearup-box-text">
						<p>{{$bdata[1]}}</p>
					</div>
				</div>

			</div>

		</div>
	</div>
</section>


<div class="bg-image-box container wow slideInRight">
	<img src="{{asset('front/images/home/bottom-desktop.png')}}" alt="" class="img-fluid " id="bottom-wide">
	<img src="{{asset('front/images/home/bottom-tablet.png')}}" alt="" class="img-fluid" id="bottom-tab">
</div>

<section class="gearup-home">
	<div class="container main-page-container">
		<div class="row ">
			<div class="col-12 col-md-8 order-2 order-sm-2 order-md-1 order-lg-1 wow fadeInUp bottom-text">

				<h1 class="bottom-heading mb-lg-5">How it works</h1>
				<div class="gearup-box-text">
						<div class="row">
							@php
								$block5 = $data['block5'];
								$bdata = explode('<br>',$block5);
							@endphp
							<div class="col-12 col-lg-6 pr-md-4">
									<img class="img-fluid favicon my-3" src="{{asset('front/images/shopping-online@10x.png')}}" alt="">
									<h2>{{$bdata[0]}}</h2>
									<div class="gearup-box-text">
										<p>{{$bdata[1]}}</p>
									</div>
							</div>
							<div class="col-12 col-lg-6 pl-md-4">
								@php
									$block6 = $data['block6'];
									$bdata = explode('<br>',$block6);
								@endphp
									<img class="img-fluid favicon my-3" src="{{asset('front/images/shopping-bag@10x.png')}}" alt="">
									<h2>{{$bdata[0]}}</h2>
									<div class="gearup-box-text">
										<p>{{$bdata[1]}}</p>
									</div>
							</div>
						</div>
						<div class="row mt-md-4">
							@php
								$block7 = $data['block7'];
								$bdata = explode('<br>',$block7);
							@endphp
								<div class="col-12 col-lg-6 pr-md-4">
										<img class="img-fluid favicon my-3" src="{{asset('front/images/parcel@10x.png')}}" alt="">
										<h2>{{$bdata[0]}}</h2>
										<div class="gearup-box-text">
											<p>{{$bdata[1]}}</p>
										</div>
								</div>
								<div class="col-12 col-lg-6 pl-md-4">
									@php
										$block8 = $data['block8'];
										$bdata = explode('<br>',$block8);
									@endphp
										<img class="img-fluid favicon my-3" src="{{asset('front/images/money@10x.png')}}" alt="">
										<h2>{{$bdata[0]}}</h2>
										<div class="gearup-box-text">
											<p>{{$bdata[1]}}</p>
										</div>
								</div>
						</div>
				</div>
			</div>

			{{-- <div class="col-12 col-md-4 order-1 order-sm-1 order-md-2 order-lg-2 wow fadeInUp">
				<div class="gear-up-img" >
					<img src="{{asset('front/images/home/bottom-mobile.png')}}" alt="" class="img-fluid " id="bottom-tab">
				</div>
			</div> --}}
		</div>
	</div>
</section>


<section class="gearup-home p-0 mt-5">
	<div class=" container main-page-container">
		<div class="row ">
			<div class="col-12 col-md-7 col-lg-6 order-md-1 wow fadeInUp bottom-text">
				<h1>Dont Miss Out</h1>
				<div class=" subscribe-form-cont">
					<!-- <div class="gearup-box-text">
						<p>Our sellers are verified and GearUp has your back with each transaction.</p>
					</div> -->
					<div class="subscribe-form-box subscribe-form-bottom">
						<!-- Begin Mailchimp Signup Form -->
						<div id="mc_embed_signup">
							<form action="https://gearupcamera.us3.list-manage.com/subscribe/post?u=986cb4a1837141fb6922b52e3&amp;id=bd3efcebdd" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
							    <div id="mc_embed_signup_scroll" class="input-group">
									<input type="email" class="form-control form-text m-0" value="" name="EMAIL" class="email" id="mce-EMAIL-two" placeholder="Email me when the app launches!" required>
								    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_986cb4a1837141fb6922b52e3_bd3efcebdd" tabindex="-1" value=""></div>
								    <div class="input-group-append ml-2">
									    <input class="btn form-btn" type="submit" value="Submit" name="subscribe" id="mc-embedded-subscribe_two" class="button">
									</div>
							    </div>
							</form>
						</div>
						<!--End mc_embed_signup-->
						<div class="error_msg" id="error_msg-two" style="display: none;">
							<p>Invalid Email Address</p>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>


<footer class="site-footer">
	<div class="container">
		<div class="row justify-content-md-between">
			<div class="col-12 col-md-auto my-auto">
				<div class="footer-col-in text-center text-md-left">
					<img class="mb-2" src="{{asset('front/images/logo-white.svg')}}">
					<p class="mb-0"><a class="text-white" href="mailto:info@gearupcamera.com">info@gearupcamera.com</a></p>
				</div>
			</div>
			<div class="col-12 col-md-auto px-0"><span class="divider-footer"></span></div>
			<div class="col-6 col-md-auto">
				<div class="footer-col-in">
					<h6>Company</h6>
					<ul class="list-unstyled mb-0">
						<li><a href="#">Our Team</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="#">Contact Us</a></li>
					</ul>
				</div>
			</div>
			<div class="col-6 col-md-auto">
				<div class="footer-col-in">
					<h6>Support</h6>
					<ul class="list-unstyled mb-0">
						<li><a href="#">Help Center</a></li>
						<li><a href="#">Terms of Service</a></li>
						<li><a href="#">Privacy Policy</a></li>
					</ul>
				</div>
			</div>
			<div class="col-12 col-md-auto">
				<div class="footer-col-in footer-col-social">
					<h6>Community</h6>
					<ul class="list-unstyled mb-0 footer-social">
						<li><a href="https://www.facebook.com/gearuptheapp/" target="_blank"><i class="fab fa-facebook-f"></i><span>Facebook</span></a></li>
						<li><a href="https://www.instagram.com/gearup.camera/" target="_blank"><i class="fab fa-instagram"></i><span>Instagram</span></a></li>
					</ul>
				</div>
			</div>
			<div class="col-12 col-md-auto mt-auto">
				<div class="footer-col-in text-center text-md-left">
					<p class="mb-0 copy-right">© 2019 GearUp LLC. All Rights reserved.</p>
				</div>
			</div>
		</div>
	</div>
</footer>
@endsection


