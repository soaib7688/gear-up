@extends('front.layouts.app')
@section('class')class="team"@endsection

@section('content')
<div class="container">
    <header class="site-header">
                <div class="row justify-content-between align-items-center">
                    <div class="col-6 col-md-3">
                        <a href="{{url('/')}}" class="logo-link">
                            <img src="{{asset('front/images/logo.svg')}}">
                        </a>
                    </div>
                    <div class="col-6 text-right d-md-none">
                        <a href="#" class="menu-toggle d-inline-flex flex-column align-items-end justify-content-between">
                            <span class="line small"></span>
                            <span class="line"></span>
                        </a>
                    </div>
                    <div class="col-md-9 text-right main-menu-container">
                        <a href="#" class="d-md-none menu-toggle d-inline-flex flex-column align-items-end justify-content-between">
                            <i class="fas fa-times"></i>
                        </a>
                        <ul class="list-unstyled mb-0 header-menu">
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li class="active"><a href="{{url('/team')}}">Our Team</a></li>
                            <li><a href="{{url('/blog')}}">Blog</a></li>
                            <li><a href="{{url('/faqs')}}">FAQ</a></li>
                            <li><a href="{{url('/contact')}}">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </header>
        </div>
<section class="team-title">
        <div class="tab-squer">

        </div>
        <div class="container">
            <div class="row justify-content-start">
                <div class="col-10">
                    <h1 class="text-capitalize">{{$data->heading}}</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="team-why">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-6">
                    <img src="{{asset('front/images/geometric shapes 1@10x.png')}}" class="img-why pb-3">
                    <h2>{{$data->left_section_heading}}</h2>
                    <p>{{$data->left_section}}</p>
                </div>
                <div class="col-12 col-lg-6">
                    <img src="{{asset('front/images/geometric shapes 2@10x.png')}}" class="img-how pb-3">
                    <h2>{{$data->right_section_heading}}</h2>
                    <p>{{$data->right_section}}</p>
                </div>
            </div>
        </div>
    </section>

    <section class="team-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="mb-3 mb-lg-5">our team</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row justify-content-center">
                    @foreach($team as $member)
                <div class="col-12 col-lg-4 col-md-6 py-3 px-5">
                    <div class="d-block team-image text-center square-image">
                        <img src="{{$member->avatar}}" class="">
                    </div>
                    <div class="team-data">
                        <h3>{{$member->name}}</h3>
                        <h5>{{$member->designation}} </h5>
                        <p>{{$member->about}}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    <footer class="site-footer">
	<div class="container">
		<div class="row justify-content-md-between">
			<div class="col-12 col-md-auto my-auto">
				<div class="footer-col-in text-center text-md-left">
					<img class="mb-2" src="{{asset('front/images/logo-white.svg')}}">
					<p class="mb-0"><a class="text-white" href="mailto:info@gearupcamera.com">info@gearupcamera.com</a></p>
				</div>
			</div>
			<div class="col-12 col-md-auto px-0"><span class="divider-footer"></span></div>
			<div class="col-6 col-md-auto">
				<div class="footer-col-in">
					<h6>Company</h6>
					<ul class="list-unstyled mb-0">
						<li><a href="#">Our Team</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="#">Contact Us</a></li>
					</ul>
				</div>
			</div>
			<div class="col-6 col-md-auto">
				<div class="footer-col-in">
					<h6>Support</h6>
					<ul class="list-unstyled mb-0">
						<li><a href="#">Help Center</a></li>
						<li><a href="#">Terms of Service</a></li>
						<li><a href="#">Privacy Policy</a></li>
					</ul>
				</div>
			</div>
			<div class="col-12 col-md-auto">
				<div class="footer-col-in footer-col-social">
					<h6>Community</h6>
					<ul class="list-unstyled mb-0 footer-social">
						<li><a href="https://www.facebook.com/gearuptheapp/" target="_blank"><i class="fab fa-facebook-f"></i><span>Facebook</span></a></li>
						<li><a href="https://www.instagram.com/gearup.camera/" target="_blank"><i class="fab fa-instagram"></i><span>Instagram</span></a></li>
					</ul>
				</div>
			</div>
			<div class="col-12 col-md-auto mt-auto">
				<div class="footer-col-in text-center text-md-left">
					<p class="mb-0 copy-right">© 2019 GearUp LLC. All Rights reserved.</p>
				</div>
			</div>
		</div>
	</div>
</footer>
@endsection
