<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>GearUp / Buy and sell camera gearupcamera</title>
    <link rel="shortcut icon" type="image/x-icon" href="http://gearupcamera.com/images/favicon.png" />
    <link rel="canonical" href="http://gearupcamera.com" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('front/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/style.css')}}">
    <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="{{asset('front/js/wow.min.js')}}"></script>
    <script src="{{asset('front/js/bootstrap.min.js')}}"></script>
</head>

<body class="home">

    <section class="" id="main">

        <!--<div class="faq-nav py-4 px-3">
            <div class="container">
                <div class="row">
                    <div class="col-4 col-lg-3 pl-0 border-secondary border-right">
                        <a href="{{url('/')}}" class="d-md-block p-lg-1">
                            <img src="{{asset('front/images/GearUp White Logo@10x.png')}}" class="img-fluid d-lg-block ">
                        </a>
                    </div>

                    <div class="col-8 col-lg-2  text-center">
                        <h5 class="d-lg-inline-block d-block text-secondary v-alignment">
                            <span class="d-inline d-lg-none">GearUp</span>
                            <span>Help Center</span>
                        </h5>
                    </div>

                    <div class="col-lg-7 pt-lg-1 d-none d-lg-block text-right" style="padding-top: 0.5rem !important;">
                        <span>Didn't find the answer?</span>
                        <a class="text-white font-weight-bold" href="{{url('/contact-us')}}">Submit a Request</a>
                    </div>
                </div>
            </div>
        </div>-->

        <div class="faq-nav py-4 px-3">
            <div class="container">
                <div class="row justify-content-between align-items-center">
                    <div class="col col-lg-5">
                        <div class="row faq-logo-col justify-content-between align-items-center justify-lg-content-left">
                            <div class="col border-right">
                                <a href="{{url('/')}}">
                                    <img class="img-fluid" src="{{asset('front/images/GearUp White Logo@10x.png')}}">
                                </a>
                            </div>
                            <div class="col text-right text-lg-left">
                                <span class="d-inline d-lg-none">GearUp</span>
                                <span>Help Center</span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-lg-5 d-none d-lg-block">
                        <span>Didn't find the answer?</span>
                        <a class="text-white font-weight-bold" href="{{url('/contact-us')}}">Submit a Request</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="faq-subNav py-4 px-3">
            <div class="container">
                <div class="row flex-column">
                    <h3  class="font-weight-bolder">How can we help?</h3>
                    <form action="{{url('/search')}}" method="POST" id="search">
                        @csrf
                        <div class="search-field">
                            @php
                            if(request('query')){
                            $query = str_replace('-',' ',request('query'));
                            }
                            @endphp
                            <i class="material-icons ">search</i>
                            <input type="text" class="form-control mb-4 pl-5 input-border" id="query" name="query"
                                value="{{@$query}}" placeholder="Search for answers">

                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="container my-5" id="faq-container">
            @if(!count($faqs))
            <h3>No results found</h3>
            @endif
            {{-- //testing --}}
            @if(isset($gen_faqs))
            <div class="row justify-content-between">
                <div class="col-12 mb-4">
                    <div class="faq-catagory pl-3 pl-lg-0">{{@$gen_faqs[0]->cat}}</div>
                </div>
                <div class="col-sm-6 col-lg-5 faq-list ">
                    <ul class="pl-3 pl-lg-0 d-none d-sm-block">
                        @php
                        $counter=1;
                        $total = count($gen_faqs);
                        $boundary = ceil($total/2);
                        @endphp
                        @foreach ($gen_faqs as $question)
                        @if($counter<=$boundary) <li>
                            <a
                                href="{{url('/faq/question/'.$question->id.'/'.Str::slug($question->question,'-'))}}">{{$question->question}}</a>
                            </li>
                            @php
                            $counter++;
                            @endphp
                            @endif
                            @endforeach
                    </ul>

                </div>

                <div class="col-sm-6 col-lg-5 faq-list ">
                    <ul class="pl-3 pl-lg-0 d-none d-sm-block">
                        <?php for ($i=$counter-1; $i < sizeof($gen_faqs); $i++) { ?>
                        <li>
                            <a
                                href="{{url('/faq/question/'.$gen_faqs[$i]->id.'/'.Str::slug($gen_faqs[$i]->question,'-'))}}">{{$gen_faqs[$i]->question}}</a>
                        </li>
                        <?php } ?>
                    </ul>

                </div>

            </div>
            @endif
            {{-- testing ends --}}
            @php
            $subcat = array();
            @endphp
            @foreach ($faqs as $key=>$value)
            @php

            // dd($faqs);
            @endphp
            @foreach ($value as $item)
            @php
            $subcats[]=$item->subcat;
            @endphp
            @endforeach
            @php
            $scats = array_unique($subcats);
            $uscats = array_values($scats);
            @endphp
            <div class="row justify-content-between">
                <div class="col-12 mb-4">
                    <div class="faq-catagory pl-3 pl-lg-0">{{$key}}</div>
                </div>
                @foreach ($uscats as $qucats)
                <div class="col-sm-6 col-lg-5 faq-list">
                    @if($key!='General')
                    <div class="d-flex justify-content-between">
                        <h4 class="pl-3 pl-lg-0">{{$qucats}}</h4>
                        <i class="material-icons d-sm-none drop-arrow">keyboard_arrow_down</i>
                    </div>
                    @endif
                    @php
                    $questions =DB::table('faqs')->select('id','question','cat','subcat')->where('cat',$key)->where('subcat',$qucats)->orderBy('order_number')->get();
                    @endphp
                    <ul class="pl-3 pl-lg-0 d-none d-sm-block">
                        @foreach ($questions as $question)
                        <li>
                            <a
                                href="{{url('/faq/question/'.$question->id.'/'.Str::slug($question->question,'-'))}}">{{$question->question}}</a>
                        </li>
                        @endforeach
                    </ul>

                </div>
                @endforeach
            </div>
            @endforeach
        </div>

        <footer class="site-footer">
            <div class="container">
                <div class="row justify-content-md-between">
                    <div class="col-12 col-md-auto my-auto">
                        <div class="footer-col-in text-center text-md-left">
                            <img class="mb-2" src="{{asset('front/images/logo-white.svg')}}">
                            <p class="mb-0"><a class="text-white"
                                    href="mailto:info@gearupcamera.com">info@gearupcamera.com</a></p>
                        </div>
                    </div>
                    <div class="col-12 col-md-auto px-0"><span class="divider-footer"></span></div>
                    <div class="col-6 col-md-auto">
                        <div class="footer-col-in">
                            <h6>Company</h6>
                            <ul class="list-unstyled mb-0">
                                <li><a href="#">Our Team</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-6 col-md-auto">
                        <div class="footer-col-in">
                            <h6>Support</h6>
                            <ul class="list-unstyled mb-0">
                                <li><a href="#">Help Center</a></li>
                                <li><a href="#">Terms of Service</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-auto">
                        <div class="footer-col-in footer-col-social">
                            <h6>Community</h6>
                            <ul class="list-unstyled mb-0 footer-social">
                                <li><a href="https://www.facebook.com/gearuptheapp/" target="_blank"><i
                                            class="fab fa-facebook-f"></i><span>Facebook</span></a></li>
                                <li><a href="https://www.instagram.com/gearup.camera/" target="_blank"><i
                                            class="fab fa-instagram"></i><span>Instagram</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-auto mt-auto">
                        <div class="footer-col-in text-center text-md-left">
                            <p class="mb-0 copy-right">© 2019 GearUp LLC. All Rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

    </section>

    <script defer src="https://use.fontawesome.com/releases/v5.7.1/js/all.js"
        integrity="sha384-eVEQC9zshBn0rFj4+TU78eNA19HMNigMviK/PU/FFjLXqa/GKPgX58rvt5Z8PLs7" crossorigin="anonymous">
    </script>
    <script src="js/script.js"></script>


    <script>
        let faqBox = $("#faq-container");
        let arrow = $(".drop-arrow");
        let dropArrow = $(".drop-arrow");


        faqBox.click((e) => {
            if (e.target.innerText === "keyboard_arrow_down") {
                for (i = 0; i < arrow.length; i++) {
                    arrow[i].parentElement.nextElementSibling.classList.add("d-none");
                    dropArrow[i].innerText = "keyboard_arrow_down";
                }
                e.target.classList.add("active");
                e.target.innerText = "keyboard_arrow_up";
                e.target.parentElement.nextElementSibling.classList.remove("d-none");
            } else if (e.target.classList.contains("active")) {
                e.target.innerText = "keyboard_arrow_down";
                e.target.parentElement.nextElementSibling.classList.add("d-none");

            }


        });
    </script>
    <script>
        function slugify(string) {
            const a = 'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;'
            const b = 'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnooooooooprrsssssttuuuuuuuuuwxyyzzz------'
            const p = new RegExp(a.split('').join('|'), 'g')

            return string.toString().toLowerCase()
                .replace(/\s+/g, '-') // Replace spaces with -
                .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
                .replace(/&/g, '-and-') // Replace & with 'and'
                .replace(/[^\w\-]+/g, '') // Remove all non-word characters
                .replace(/\-\-+/g, '-') // Replace multiple - with single -
                .replace(/^-+/, '') // Trim - from start of text
                .replace(/-+$/, '') // Trim - from end of text
        }
        $('#search').submit(function () {
            let str = $('#query').val();
            str = slugify(str);
            if (str === "" || str === null || str === undefined) {
                return false;
            }
            window.location.href = '/search?query=' + str;
            return false;
        });
    </script>

</body>

</html>
