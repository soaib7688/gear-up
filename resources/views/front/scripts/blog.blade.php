
<script>
        let vu = new Vue({
            el: '#VueJs',
            data: {
                posts: [],
                categories:[],
                limit:6,
                loader:true,
            },
            methods: {

                fetchCategories() {
                    let self = this;
                    const auth = {
                        headers: {
                            'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
                        }
                    }
                    var link = 'https://gearupcamera-api.herokuapp.com/v1/blog/category';
                    self.$http.get(link,auth).then(function (response) {
                        self.categories = response.body;
    
                    }, function (error) {
                        json = JSON.stringify(error.body.message);
                        toastr.error(json);
                    });
    
                },

                fetchPosts() {
                    document.querySelector('.filter-active').classList.remove('filter-active')
                    $('#all').addClass('filter-active');
                    let self = this;
                    const auth = {
                        headers: {
                            'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
                        }
                    }
                    var link = 'https://gearupcamera-api.herokuapp.com/v1/blog/post';
                    self.$http.get(link,auth).then(function (response) {
                        self.loader=false;
                        self.posts = response.body;
                        console.log(self.posts);
                        
    
                    }, function (error) {
                        json = JSON.stringify(error.body.message);
                        toastr.error(json);
                    });
    
                },

                getUrl(url){
                    var lastPart = url.split("/").pop();
                    return url.replace(lastPart,'/thumbs/'+lastPart);
                    
                    
                },

                isLast(){
                    if(this.posts.length>this.limit){
                        return false;
                    }
                    return true;
                },

                fetchByCategory(id){
                    document.querySelector('.filter-active').classList.remove('filter-active')
                    $('#'+id).addClass('filter-active');
                    let self = this;
                    const auth = {
                        headers: {
                            'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
                        }
                    }
                    var link = 'https://gearupcamera-api.herokuapp.com/v1/blog/category/'+id+'/post';
                    self.$http.get(link,auth).then(function (response) {
                        self.loader=false;
                        self.posts = response.body;
                        console.log(self.posts);
                        
    
                    }, function (error) {
                        json = JSON.stringify(error.body.message);
                        toastr.error(json);
                    });
                },
                setDate(date){
                    const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
                    let current_datetime = new Date(date)
                    let formatted_date = current_datetime.getDate() + " " + months[current_datetime.getMonth()] + " " + current_datetime.getFullYear()
                    
                    return formatted_date;
                }

            },
            
    
    
    
            mounted() {
                this.fetchCategories();
                this.fetchPosts();
            },
        }); 
    </script>

