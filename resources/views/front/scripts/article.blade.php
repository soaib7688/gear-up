
<script>
        let vu = new Vue({
            el: '#VueJs',
            data: {
                comments:[],
            },
            methods: {
                postComment(){
                    let self=this;
                    let id = $('#post_id').val();
                    let comment = $('#comment').val();
                    let name = $('#name').val();
                    let email = $('#email').val();
                    let form = {
                        "name":name,
                        "email":email,
                        "post_id":id,
                        "comment":comment
                    };

                    const auth = {
                        headers: {
                            'Content-Type':'application/json',
                            'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
                        }
                    }
                    var link = 'https://gearupcamera-api.herokuapp.com/v1/blog/comment';
                    self.$http.post(link,form,auth).then(function (response) {
                        toastr.success('Comment Posted Successfully');
                         $('#comment').val('');
                         $('#name').val('');
                         $('#email').val('');
                        this.getComments();
    
                    }, function (error) {
                        json = JSON.stringify(error.body.message);
                        toastr.error(json);
                    });
                    
                },

                getComments(){
                    let self=this;
                    let id = $('#post_id').val();

                    const auth = {
                        headers: {
                            'Content-Type':'application/json',
                            'x-request-jwt': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
                        }
                    }
                    var link = 'https://gearupcamera-api.herokuapp.com/v1/blog/post/'+id+'/comments';
                    self.$http.get(link,auth).then(function (response) {
                        self.comments = response.body;
    
                    }, function (error) {
                        json = JSON.stringify(error.body.message);
                        toastr.error(json);
                    });
                },
                setDate(date){
                    const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
                    let current_datetime = new Date(date)
                    let formatted_date = current_datetime.getDate() + " " + months[current_datetime.getMonth()] + " " + current_datetime.getFullYear()
                    
                    return formatted_date;
                }
            },
            
            // computed: {
            //     orderedComments: function () {
            //         return _.orderBy(this.comments, 'created_at')
            //         }
            //     },
    
    
            mounted() {
                this.getComments();
            },
        }); 
    </script>

