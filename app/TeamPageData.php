<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamPageData extends Model
{
    protected $guarded =[];
    protected $table = 'team_page_content';
}
