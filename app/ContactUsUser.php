<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUsUser extends Model
{
    protected $fillable = [
        'name',
        'email',
		'message',
    ];
}
