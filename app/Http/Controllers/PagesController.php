<?php

namespace App\Http\Controllers;

use App\ContactUsUser;
use App\Faq;
use App\Team;
use App\TeamPageData;
use App\User;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DB;

use function GuzzleHttp\json_decode;

class PagesController extends Controller
{
    public function index()
    {
        $data = DB::table('homepages')->select('block1','block2','block3','block4','block5','block6','block7','block8')->first();
        $data= collect($data);
        return view('front.index',compact('data'));
    }

    public function team()
    {
        $team = Team::all();
        $data = TeamPageData::first();
        return view('front.team',compact('team','data'));
    }

    public function contact()
    {
        return view('front.contact');
    }

    public function blog()
    {
        return view('front.blog');
    }

    public function blogArticle($slug)
    {
        $exp = explode("-",$slug);
        $last_id = end($exp);
        $page_id = substr($last_id,0,1);
        $post_id = substr($last_id,1,1000);
        $headers = [
            'Content-Type'=>'application/json',
            'x-request-jwt' => 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
        ];
        $client = new Client([
            'headers' => $headers
        ]);

        $r = $client->request('GET', 'https://gearupcamera-api.herokuapp.com/v1/blog/post/'.$post_id);
        $response = $r->getBody()->getContents();
        return view('front.article',['post'=>json_decode($response)]);
    }

    public function faqs()
    {
        $gen_faqs = Faq::where('cat','General')->orderBy('order_number')->get();
        $faqs = Faq::where('cat','!=','General')->get();
        $faqq = $faqs->sortBy('order_number');
        $faqs = $faqq->groupBy('cat');
        
        return view('front.faqs',compact('faqs','gen_faqs'));
    }

    public function singlefaq($id)
    {
        $data = Faq::findOrFail($id);
        $cat = $data->cat;
        $related = Faq::where('cat',$cat)->inRandomOrder()->get();
        return view('front.faq-question',compact('data','related'));
    }

    public function contactususers() {
        request()->validate([
            'name' => ['required', 'regex:/^[A-ZÀÂÇÉÈÊËÎÏÔÛÙÜŸÑÆŒa-zàâçéèêëîïôûùüÿñæœ ]+$/'],
            'email' => ['required', 'string', 'email', 'max:50'],
            'message' => ['required', 'string'],
        ]);
        $response = ContactUsUser::create([
            'name' => request('name'),
            'email' => request('email'),
            'message' => request('message'),
        ]);
        if ($response) {
            $to = 'info@gearupcamera.com';
            $subject = 'Gearup Contact Request';
            $message = '
                  <html>
                      <head>
                          <title>Call me back</title>
                      </head>
                      <body>
                          <p><b>Name:</b> '.request('name').'</p>
                          <p><b>Email Address:</b> '.request('email').'</p>
                          <p><b>Message:</b> '.request('message').'</p>                     
                      </body>
                  </html>'; 
            $headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
          $headers .= "From: Site <info@gearupcamera.com>\r\n"; 
         
          mail($to, $subject, $message, $headers);

        echo json_encode(array('status' => 'success'));
        } else {
            echo json_encode(array('status' => 'error'));
        }
    }

    
}
