<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class PostController extends Controller
{
    public function addpost()
    {
        return view('admin.add_post');
    }

    public function viewPosts()
    {
        return view('admin.view_post');
    }

    public function editpost($slug)
    {
        $exp = explode("-",$slug);
        $last_id = end($exp);
        $page_id = substr($last_id,0,1);
        $post_id = substr($last_id,1,1000);
        $headers = [
            'Content-Type'=>'application/json',
            'x-request-jwt' => 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWQ4YzhlMTNhODk5MDI0ZjQwZmQ3YmI1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiZ2VhclVwIGFkbWluIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNTY5NTI0NDg4fQ.854VbG8iughNTzg0cx-DknXziaIGBnDhDgt9mcmuOTA'
        ];
        $client = new Client([
            'headers' => $headers
        ]);

        $r = $client->request('GET', 'https://gearupcamera-api.herokuapp.com/v1/blog/post/'.$post_id);
        $response = $r->getBody()->getContents();
        
        return view('admin.add_post',['post'=>json_decode($response)]);
    }
}
