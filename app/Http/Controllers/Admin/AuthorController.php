<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

use function GuzzleHttp\json_decode;

class AuthorController extends Controller
{
    public function authorList()
    {
      
        return view('admin.author');
    }
}
