<?php

namespace App\Http\Controllers\Admin;

use App\Faq;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Team;
use App\TeamPageData;
use DB;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function dashboard()
    {
        return view('admin.dashboard');
    }

    public function homePage()
    {
        $content = DB::table('homepages')->select('block1','block2','block3','block4','block5','block6','block7','block8','block9')->first();

        return view('admin.home-page',['content'=>$content]);
    }

    public function saveHomePage()
    {
        if(request('block')==1){
            request()->validate([
                'heading'=>'required',
                'content'=>'required'
            ]);
            $title = request('heading');
            $content = request('content');
            $data = $title.'<br>'.$content;

            DB::table('homepages')->where('id',1)->update([
                'block1'=>$data,
            ]);

        return back()->with('flash_message','Settings Saved Successfully');
        }

        if(request('block')==2){
            request()->validate([
                'heading'=>'required',
                'content'=>'required'
            ]);
            $title = request('heading');
            $content = request('content');
            $data = $title.'<br>'.$content;

            DB::table('homepages')->where('id',1)->update([
                'block2'=>$data,
            ]);

        return back()->with('flash_message','Settings Saved Successfully');            
        }

        if(request('block')==3){
            request()->validate([
                'heading'=>'required',
                'content'=>'required'
            ]);
            $title = request('heading');
            $content = request('content');
            $data = $title.'<br>'.$content;

            DB::table('homepages')->where('id',1)->update([
                'block3'=>$data,
            ]);

        return back()->with('flash_message','Settings Saved Successfully');                        
        }

        if(request('block')==4){
            request()->validate([
                'heading'=>'required',
                'content'=>'required'
            ]);
            $title = request('heading');
            $content = request('content');
            $data = $title.'<br>'.$content;

            DB::table('homepages')->where('id',1)->update([
                'block4'=>$data,
            ]);

        return back()->with('flash_message','Settings Saved Successfully');                                    
        }

        if(request('block')==5){
            request()->validate([
                'heading'=>'required',
                'content'=>'required'
            ]);
            $title = request('heading');
            $content = request('content');
            $data = $title.'<br>'.$content;

            DB::table('homepages')->where('id',1)->update([
                'block5'=>$data,
            ]);

        return back()->with('flash_message','Settings Saved Successfully');                                                
        }

        if(request('block')==6){
            request()->validate([
                'heading'=>'required',
                'content'=>'required'
            ]);
            $title = request('heading');
            $content = request('content');
            $data = $title.'<br>'.$content;

            DB::table('homepages')->where('id',1)->update([
                'block6'=>$data,
            ]);

        return back()->with('flash_message','Settings Saved Successfully');                                                           
        }

        if(request('block')==7){
            request()->validate([
                'heading'=>'required',
                'content'=>'required'
            ]);
            $title = request('heading');
            $content = request('content');
            $data = $title.'<br>'.$content;

            DB::table('homepages')->where('id',1)->update([
                'block7'=>$data,
            ]);

        return back()->with('flash_message','Settings Saved Successfully');
        }

        if(request('block')==8){
            request()->validate([
                'heading'=>'required',
                'content'=>'required'
            ]);
            $title = request('heading');
            $content = request('content');
            $data = $title.'<br>'.$content;

            DB::table('homepages')->where('id',1)->update([
                'block8'=>$data,
            ]);

        return back()->with('flash_message','Settings Saved Successfully');                                                           
            
        }

        if(request('block')==9){
            request()->validate([
                'heading'=>'required',
                'content'=>'required'
            ]);
            $title = request('heading');
            $content = request('content');
            $data = $title.'<br>'.$content;

            DB::table('homepages')->where('id',1)->update([
                'block9'=>$data,
            ]);

        return back()->with('flash_message','Settings Saved Successfully');
        }

        
    }

    public function faqPage()
    {
        $data = DB::table('faqs')->select('*')->orderBy('order_number')->get();
        $parent = DB::table('faqs_categories')->select('title','id')->where('parent_id',0)->get();

        return view('admin.faq-page',compact('parent','data'));
    }

    public function faqCategoryPage()
    {
        $parent = DB::table('faqs_categories')->select('title','id')->where('parent_id',0)->get();
        $categories = DB::table('faqs_categories')->select('title','id')->get();
        return view('admin.faq-category-page',compact('parent','categories'));        
    }

    public function postFaqCategory()
    {
        request()->validate([
            'title'=>'required'
        ]);
        $parent = request('parent');
            if(is_null($parent)){
                $parent=0;
            }
            $title = request('title');

        if(request('id')){
            DB::table('faqs_categories')->where('id',request('id'))->update([
                'title'=>$title,
                'parent_id'=>$parent
            ]);

        }else{
            
    
            DB::table('faqs_categories')->insert([
                'title'=>$title,
                'parent_id'=>$parent
            ]);
        }
        

        return back()->with('flash_message','Settings Saved Successfully');
    }

    public function deleteFaqCategory($id)
    {
        DB::table('faqs_categories')->delete($id);
        return back()->with('flash_message','Category Deleted Successfully');        
    }

    public function editFaqCategory($id)
    {
        $parent = DB::table('faqs_categories')->select('title','id')->where('parent_id',0)->get();
        $categories = DB::table('faqs_categories')->select('title','id')->get();        
        $faq_category = DB::table('faqs_categories')->where('id',$id)->first();
        return view('admin.faq-category-page',compact('parent','categories','faq_category'));
    }

    public function getsubcats()
    {
        $html="";
        $id = request('id');
        $subcats = DB::table('faqs_categories')->select('*')->where('parent_id',$id)->get();
        
        foreach ($subcats as $subcat) {
            $html.="<option value=".$subcat->id.">".$subcat->title."</option>";
        }

        return $html;
    }

    public function savefaqs()
    {
        request()->validate([
            'category'=>'required',
            'question'=>'required',
            'answer'=>'required'
        ]);
        $category = request('category');
        $catdata = DB::table('faqs_categories')->select('*')->where('id',$category)->first();
        $cat = $catdata->title;
        if($cat!='General'){
            $subcategory = request('sub-category');
            $subcatdata = DB::table('faqs_categories')->select('*')->where('id',$subcategory)->first();
            $subcat = $subcatdata->title;
        }else{
            $subcat ="";
        }
        $question = request('question');
        $answer = request('answer');
        if(request('id')==""){
            DB::table('faqs')->insert([
                'cat'=>$cat,
                'subcat'=>$subcat,
                'question'=>$question,
                'answer'=>$answer,
            ]);
        }else{
            DB::table('faqs')->where('id',request('id'))->update([
                'cat'=>$cat,
                'subcat'=>$subcat,
                'question'=>$question,
                'answer'=>$answer,
            ]);
        }
        return back()->with('flash_message','Settings Saved Successfully');
    }

    public function saveOrder()
    {
        $orders = request('order');
		foreach($orders as $k => $v){
			$page = DB::table('faqs')->find($v);
			if($page) {
                DB::table('faqs')->where('id',$v)->update(['order_number'=>$k]);
			}
			
		}
	    return back()->with('flash_message','Yours settings are saved successfully'); 
    }

    public function showTeam()
    {
        $data = DB::table('team_page_content')->select(['id','heading','right_section_heading','left_section_heading','right_section','left_section'])->first();
        $team = Team::all();
        return view('admin.team',compact('team','data'));
    }

    public function saveTeam()
    {
        request()->validate([
            'name'=>'required',
            'designation'=>'required',
            'about'=>'required',
            'filepath'=>'required'
        ]);
        if(request('id')){
            $member = Team::find(request('id'));
            $member->name = request('name');
            $member->email = request('email');
            $member->designation = request('designation');
            $member->about = request('about');
            $member->avatar = request('filepath');
            $member->save();
        }else{
            Team::create([
                'name'=>request('name'),
                'email'=>request('email'),
                'designation'=>request('designation'),
                'about'=>request('about'),
                'avatar'=>request('filepath'),
            ]);
        }
        

	    return back()->with('flash_message','Yours settings are saved successfully');         
        
    }

    public function deleteteam_member(Team $member)
    {
        $member->delete();
        return back()->with('flash_message','Member deleted Successfully');

    }

    public function editfaq($id)
    {
        $faq = Faq::findOrFail($id);
        $data = DB::table('faqs')->select('*')->orderBy('order_number')->get();
        $parent = DB::table('faqs_categories')->select('title','id')->where('parent_id',0)->get();
        return view('admin.faq-page',['faq'=>$faq,'data'=>$data,'parent'=>$parent]);
    }

    public function deletefaq(Faq $id)
    {
        $id->delete();
        return back()->with('flash_message','Settings Saved Successfully');
    }

    public function editteam_member(Team $member)
    {
        $team = Team::all();
        return view('admin.team',compact('member','team'));
    }

    public function scriptPageShow()
    {
        $data = DB::table('script')->first();
        return view('admin.scriptPage',compact('data'));
    }

    public function saveScript()
    {
        $data = DB::table('script')->first();
        $script = request('script');
        if(!$data){
            DB::table('script')->insert(['script'=>$script]);
        }else{
            $id = $data->id;
            DB::table('script')->where('id',$id)->update(['script'=>$script]);
        }
        
        return back()->with('flash_message','Settings Saved Successfully');
    }

    public function saveFooterScript()
    {
        $data = DB::table('script')->first();
        $script = request('footerscript');
        if(!$data){
            DB::table('script')->insert(['footer_script'=>$script]);
        }else{
            $id = $data->id;
            DB::table('script')->where('id',$id)->update(['footer_script'=>$script]);
        }
        
       return back()->with('flash_message','Settings Saved Successfully');
    }

    public function saveBodyScript()
    {
        $data = DB::table('script')->first();
        $script = request('bodyscript');
        if(!$data){
            DB::table('script')->insert(['body_script'=>$script]);
        }else{
            $id = $data->id;
            DB::table('script')->where('id',$id)->update(['body_script'=>$script]);
        }
        
        return back()->with('flash_message','Settings Saved Successfully');
    }

    public function saveTeamHeading()
    {
        request()->validate([
            'heading'=>'required'
        ]);
        $heading = request('heading');
        if(request('id')){
            $data = TeamPageData::first();
            $data->heading = $heading;
            $data->save();
        }else{
            TeamPageData::create([
                'heading',$heading,
            ]);
        }

        return back()->with('flash_message','Settings Saved Successfully');

    }

    public function saveRightHeading()
    {
        request()->validate([
            'right_section_heading'=>'required',
            'right_section_content'=>'required',
        ]);
        $rightSectionHeading = request('right_section_heading');
        $rightSectionContent = request('right_section_content');
        if(request('id')){
            $data = TeamPageData::first();
            $data->right_section = $rightSectionContent;
            $data->right_section_heading = $rightSectionHeading;
            $data->save();
        }else{
            TeamPageData::create([
                'right_section',$rightSectionContent,
                'right_section_heading',$rightSectionHeading,
            ]);
        }
        return back()->with('flash_message','Settings Saved Successfully');        
    }

    public function saveLeftHeading()
    {
        request()->validate([
            'left_section_heading'=>'required',
            'left_section_content'=>'required',
        ]);
        $leftSectionHeading = request('left_section_heading');
        $leftSectionContent = request('left_section_content');
        if(request('id')){
            $data = TeamPageData::first();
            $data->left_section = $leftSectionContent;
            $data->left_section_heading = $leftSectionHeading;
            $data->save();
        }else{
            TeamPageData::create([
                'left_section',$leftSectionContent,
                'left_section_heading',$leftSectionHeading,
            ]);
        }
        return back()->with('flash_message','Settings Saved Successfully');                
    }
}
