<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SearchController extends Controller
{
    public function search()
    {
        $slug = request('query');
        $query = str_replace('-',' ',$slug);
        // dd($query);
        // request()->validate([
        //     'query'=>'required:string'
        // ]);
        $query = request('query');

        $faqs = DB::select("select * from faqs WHERE MATCH(question,answer)
                AGAINST('$query' IN NATURAL LANGUAGE MODE) or question like '%$query%' or answer like '%$query%'");

        $faqs = collect($faqs);
        $faqs = $faqs->groupBy('cat');
        return view('front.faqs',['faqs'=>$faqs]);
    }
}
