<?php

namespace App\Http\Middleware;

use Closure;

class UrlHandling
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $url = $request->fullUrl();
        // dd(str_replace('%20','-',$request->segment(4)));
        return $next($request);
    }
}
