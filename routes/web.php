<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// admin routes

Route::prefix('admin')->group(function(){
    Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login','Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/dashboard','Admin\AdminController@dashboard')->name('admin.dashboard');
    Route::get('/add-post','Admin\PostController@addpost');
    Route::get('/post/edit/{slug}','Admin\PostController@editpost');
    Route::get('/view-posts','Admin\PostController@viewPosts');
    Route::get('/author','Admin\AuthorController@authorList');
    Route::post('/author','Admin\AuthorController@store');
    Route::get('/category','Admin\CategoryController@category');
    Route::get('/pages/home-page','Admin\AdminController@homePage');
    Route::post('/pages/home-page','Admin\AdminController@saveHomePage');
    Route::get('/pages/faq-page','Admin\AdminController@faqPage');
    Route::get('/pages/faq-category-page','Admin\AdminController@faqCategoryPage');
    Route::post('/faq/categories','Admin\AdminController@postFaqCategory');
    Route::get('/faqCategory/delete/{id}','Admin\AdminController@deleteFaqCategory');
    Route::get('/faq-category/edit/{id}','Admin\AdminController@editFaqCategory');
    Route::get('/script-setting','Admin\AdminController@scriptPageShow');
    Route::post('/saveScript','Admin\AdminController@saveScript');
    Route::post('/saveFooterScript','Admin\AdminController@saveFooterScript');
    Route::post('/saveBodyScript','Admin\AdminController@saveBodyScript');
    Route::post('/savefaqs','Admin\AdminController@savefaqs');
    Route::post('/saveOrder','Admin\AdminController@saveOrder');
    Route::get('/team/create','Admin\AdminController@showTeam');
    Route::post('/saveTeam','Admin\AdminController@saveTeam');
    Route::post('/saveTeamHeading','Admin\AdminController@saveTeamHeading');
    Route::post('/saveTeamRightData','Admin\AdminController@saveRightHeading');
    Route::post('/saveTeamLeftData','Admin\AdminController@saveLeftHeading');
    Route::get('/faq/edit/{id}','Admin\AdminController@editfaq');
    Route::get('/faq/delete/{id}','Admin\AdminController@deletefaq');
    Route::get('/team-member/edit/{member}','Admin\AdminController@editteam_member');
    Route::get('/team-member/delete/{member}','Admin\AdminController@deleteteam_member');
    Route::get('/logout','Auth\AdminLoginController@logout')->name('admin.logout');
});
Route::get('/getSubCats','Admin\AdminController@getsubcats');
Auth::routes();

//front routes
// Route::get('/user/logout', 'Auth/LoginController@userlogout')->name('user.logout');
Route::get('/','PagesController@index');
Route::get('/team','PagesController@team');
Route::get('/contact','PagesController@contact');
Route::get('/faqs','PagesController@faqs');
Route::get('/faq/question/{id}/{question}','PagesController@singlefaq');
Route::get('/blog','PagesController@blog');
Route::get('/blog/{slug}','PagesController@blogArticle');
Route::get('/search','SearchController@search');
Route::get('/contact-us',function(){
    return view("front.request-faq") ;
});

Route::post('/contactususers','PagesController@contactususers');

Route::post('/submit-query',function(){
    dd(request()->all());
});