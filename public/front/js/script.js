$( document ).ready(function() {

  var menu_toggle = $('.menu-toggle');
	var mobile_cont = $('.main-menu-container');
		
	menu_toggle.click( function(e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
        var status = mobile_cont.hasClass('active');
      if(status){
        mobile_cont.removeClass('active');
      }else{
        mobile_cont.addClass('active');
      }
  });

  wow = new WOW(
      {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
    )
    wow.init();

    $("#mc-embedded-subscribe").click(function(e){
        var testEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var value = document.getElementById('mce-EMAIL').value;
        var error = document.getElementById('error_msg');
        if(testEmail.test(value)){
          $(error).css({'display':'none'});
          return (true);
        } 
        else {
          $(error).css({'display':'block'});
          e.preventDefault();
      }
    });


    $("#mc-embedded-subscribe_two").click(function(e){
        var testEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var value = document.getElementById('mce-EMAIL-two').value;
        var error = document.getElementById('error_msg-two');
        if(testEmail.test(value)){
          $(error).css({'display':'none'});
          return (true);
        } 
        else {
          $(error).css({'display':'block'});
          e.preventDefault();
      }
    });


// Contact Form submit
 
  // $("#contact-form-ajax").submit(function (e) {
  //   var form = $(this).attr('id');
  //   var error = $(form).find('p');
  //     e.preventDefault();
  //     var form_data = $(this).serialize(); 
  //     //$('#form-sub-msg').text('Your message has been successfuly sent to GearUp!');
  //     $('#form-recponce').addClass('alert-success active');
  //     $.ajax({
  //       type: "POST", 
  //       url: "call-form.php",
  //       dataType: "json", // Add datatype
  //       data: form_data
  //     }).done(function (data) {
  //       //console.log(data);
  //     }).fail(function (data) {
  //         //console.log(data);
  //     });
  //     $( '#contact-form-ajax' ).each(function(){
  //         this.reset();
  //     });
  // }); 

  $('.team-loader').click(function() {
    $('.team-profile').addClass('d-block');
    $('.team-loader').addClass('d-none');
  });
});
